$("input[type=phone]").keypress(function (event) {
    var regex = new RegExp("^[0-9\]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
});

$("input[rules=number-only]").keypress(function (event) {
    var regex = new RegExp("^[0-9\]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
});

$(".date").keypress(function(event){
  event.preventDefault();
  return false;
});
