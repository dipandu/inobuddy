<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Page Loader Routes
Route::get('/', 'WebController@index');
Route::get('login', 'WebController@login');
Route::get('signup', 'WebController@signup');
Route::get('tenders', 'TenderController@browseTenders');
Route::get('tenders/{tender_slug}', 'TenderController@viewTender');

//Social Authentication
Route::get('auth/google-login', 'ApiAuthController@redirectToProvider');
Route::get('auth/google-login/callback', 'ApiAuthController@handleProviderCallback');


//Internal API Routes
Route::get('api/check-user-exist', 'ApiUserController@check_user_exist');
Route::post('api/signup', 'ApiUserController@signup');
Route::post('api/login', 'ApiUserController@login_authentication');
Route::post('api/save-user-data', 'ApiUserController@save_user_data');
Route::post('api/tender/change-status', 'TenderController@changeStatus');
Route::post('api/tender/delete-tender-file', 'TenderController@deleteFile');
Route::post('api/tender/update-tender', 'TenderController@updateTender');
Route::post('api/bids/place-bid', 'BidsController@placeBid');
Route::post('api/bids/change-status', 'BidsController@changeStatus');

//Profile Loader Routes
Route::get('profile', function(){
  // dd(decrypt(session()->get('login_token')));
  if(session()->has('login_token')){
    return redirect(url('profile/'.decrypt(session()->get('login_token'))));
  }

 return abort(404);
});
Route::get('profile/edit', 'ProfileController@profileEditor');
Route::get('profile/{username}', 'ProfileController@profile');
Route::get('dashboard/tenders', 'DashboardController@tenders');
Route::get('manage/tender/{tender_slug}', 'DashboardController@manageTender');
Route::get('dashboard/bids', 'DashboardController@bids');

//Testing Routes
Route::get('logout', function(){
  session()->forget('login_token');
  return redirect(url('/'));
});

//Storage Routes
Route::get('storage/{uid}/ppic/{filename}', function($uid, $filename){
  $path = storage_path($uid.'/ppic/'.$filename);
  // dd($path);
  if (!File::exists($path)) {
    abort(404);
  }

  $file = File::get($path);
  $type = File::mimeType($path);

  $response = Response::make($file, 200);
  $response->header("Content-Type", $type);

  return $response;
});
Route::get('storage/{uid}/tenders/{tender_id}/{filename}', function($uid, $tender_id, $filename){
  $path = storage_path($uid.'/tenders/'.$tender_id.'/'.$filename);
  // dd($path);
  if (!File::exists($path)) {
    abort(404);
  }

  $file = File::get($path);
  $type = File::mimeType($path);

  $response = Response::make($file, 200);
  $response->header("Content-Type", $type);

  return $response;
});

//Tender Routes
Route::get('post-tender', 'TenderController@postTender');
Route::post('save-tender', 'TenderController@saveTender');
Route::get('test', 'TenderController@test');
