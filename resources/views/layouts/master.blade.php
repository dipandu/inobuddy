<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @yield('meta_tag')

    <title>@yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('public/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{{url('public/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="https://unpkg.com/@bootstrapstudio/bootstrap-better-nav/dist/bootstrap-better-nav.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
    <link href="{{url('public/css/agency.min.css')}}" rel="stylesheet">
    <link href="{{url('public/css/bootstrap-social.css')}}" rel="stylesheet">
    <link href="{{url('public/css/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link href="{{url('public/css/inobuddy.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    @yield('custom_style')

  </head>

  <body id="page-top">

    <!-- Navigation -->
    @yield('navbar')

    <!-- Content -->
    @yield('content')

    <!-- Footer -->

    <!-- Bootstrap core JavaScript -->
    <script src="{{url('public/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{url('public/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{url('public/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{url('public/js/bootstrap-tagsinput.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Contact form JavaScript -->
    <script src="{{url('public/js/jqBootstrapValidation.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
    <script src="{{url('public/js/contact_me.js')}}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{url('public/js/agency.min.js')}}"></script>
    <script src="{{url('public/js/inobuddy.js')}}"></script>
    <script src="https://unpkg.com/@bootstrapstudio/bootstrap-better-nav/dist/bootstrap-better-nav.min.js"></script>
    @yield('custom_script')

  </body>

  <footer>
    <div class="container">
      <div class="row footer-site-link">
        <div class="col-12 col-md-3 pb-2">
          <h3><span style="color:#fed136;">INO</span>BUDDY</h3>
          <span>Save your time and turns your ideas to reality.</span>
        </div>
        <div class="col-12 col-md-3 text-left">
          <h6>Website</h6>
          <ul>
            <li>Browse Tenders</li>
            <li>Tender Management</li>
          </ul>
        </div>
        <div class="col-12 col-md-3 text-left pb-2">
          <h6>About</h6>
          <ul>
            <li>About us</li>
            <li>How it Works</li>
            <li>Team</li>
            <li>Sitemap</li>
          </ul>
        </div>
        <div class="col-12 col-md-3 text-left pb-2">
          <h6>Get in Touch</h6>
          <ul>
            <li>Support</li>
            <li>Careers</li>
            <li>Contact us</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <span class="copyright">Copyright &copy; Your Website 2018</span>
        </div>
        <div class="col-md-4">
          <ul class="list-inline social-buttons">
            <li class="list-inline-item">
              <a href="#">
                <i class="fa fa-twitter"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fa fa-facebook"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fa fa-linkedin"></i>
              </a>
            </li>
          </ul>
        </div>
        <div class="col-md-4">
          <ul class="list-inline quicklinks">
            <li class="list-inline-item">
              <a href="#">Privacy Policy</a>
            </li>
            <li class="list-inline-item">
              <a href="#">Terms of Use</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

</html>
