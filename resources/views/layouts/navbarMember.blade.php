@section('navbar')
<nav class="navbar navbar-expand-lg navbar-dark custom-navbar" id="mainNav">
  <div class="container">
    <a class="navbar-brand" href="<?= URL::to('/'); ?>">DiPandu</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      Menu
      <i class="fa fa-bars"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav text-uppercase mr-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Find Your Buddy
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="{{url('post-tender')}}">Post a Tender</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Find Tenders
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            {{-- <a class="dropdown-item" href="#">Matching Tender</a> --}}
            <a class="dropdown-item" href="{{url('tenders')}}">Browse Tender</a>
          </div>
        </li>
      </ul>
      <ul class="navbar-nav text-uppercase ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            My Profile
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="{{url('profile')}}">Profile</a>
            <hr />
            <a class="dropdown-item" href="{{url('dashboard/tenders')}}">My Tenders</a>
            <a class="dropdown-item" href="{{url('dashboard/bids')}}">My Bids</a>
            <hr />
            <a class="dropdown-item" href="#">Settings</a>
            <a class="dropdown-item" href="{{url('logout')}}">Logout</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>
@endsection
