@section('navbar')
<nav class="navbar navbar-expand-lg navbar-dark custom-navbar" id="mainNav">
  <div class="container">
    <a class="navbar-brand" href="<?= URL::to('/'); ?>">DiPandu</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      Menu
      <i class="fa fa-bars"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav text-uppercase mr-auto">
      </ul>
      <ul class="navbar-nav text-uppercase ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="#">How It Works</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= URL::to('/login'); ?>">Log In</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= URL::to('/signup'); ?>">Sign Up</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
@endsection
