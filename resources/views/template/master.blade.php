<!DOCTYPE html>
<html dir="ltr">
<head>
  <!-- meta -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="business finance, consultancy, multi page, corporate, ecommerce shop, one page, html, financial, money, business, responsive, arrowthemes" />
  <meta name="description" content="Finance and Consultancy Business HTML Template" />
  <meta name="author" content="arrowthemes">
  <title>@yield('title')</title>

  <!-- fav icon -->
  <link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

  <!-- css -->
  <link rel="stylesheet" href="css/plyr.css">
  <link rel="stylesheet" href="css/marigold/bootstrap.css">
  <link rel="stylesheet" href="css/marigold/theme.css">
  <link rel="stylesheet" href="css/custom.css">
</head>

<body id="tm-container">
  <div class="tm-container">

    <!-- preloader -->
    <div class="tm-preload">
      <div class="spinner"></div>
    </div>

    <!-- to top scroller -->
    <div class="uk-sticky-placeholder">
      <div data-uk-smooth-scroll data-uk-sticky="{top:-500}">
        <a class="tm-totop-scroller uk-animation-slide-bottom" href="#" ></a>
      </div>
    </div>

    <!-- toolbar -->
    <div id="tm-toolbar" class="tm-toolbar uk-hidden-small">
      <div class="uk-container uk-container-center uk-clearfix">

        <!-- toolbar left -->
        <div class="uk-float-left">
          <div>
            <ul class="uk-subnav uk-subnav-line">
              <li><a class="tm-modal-link" href="#modal-a" data-uk-modal="{center:true}">Login</a></li>
            </ul>
          </div>

          <div>
            <div class="uk-button-dropdown" data-uk-dropdown="">
                <a class="uk-button-link uk-button" href="#" target="_self">English <i class="uk-icon-caret-down"></i></a>
                <div class="uk-dropdown uk-dropdown-small uk-color uk-dropdown-bottom">
                    <ul class="uk-nav uk-nav-dropdown">
                        <li><a href="">English</a></li>
                        <li><a href="">Korean</a></li>
                    </ul>
                </div>
            </div>
          </div>
        </div>

        <!-- toolbar right -->
        <div class="uk-float-right">

          <div>
            <a href="#" class="uk-icon-button uk-icon-linkedin" target="_blank"></a>
            <a href="#" class="uk-icon-button uk-icon-facebook" target="_blank"></a>
            <a href="#" class="uk-icon-button uk-icon-instagram" target="_blank"></a>
          </div>

          <div class="uk-hidden-small">
            <ul class="uk-list list-icons">
              <li><i class="uk-icon-btc"></i>1 BTC = 16.552.000 WON </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <!-- header -->
    <div class="tm-header tm-header-right" data-uk-sticky>
      <div class="uk-container uk-container-center">
        <div class="uk-flex uk-flex-middle uk-flex-space-between">

          <!-- logo -->
          <a class="tm-logo uk-hidden-small" href="index.html">
            <img src="images/demo/default/logo/logo.png" width="180" height="50" alt="demo">
          </a>

          <!-- small logo -->
          <a class="tm-logo-small uk-visible-small" href="index.html">
            <img src="images/demo/default/logo/logo-small.png" width="140" height="40" alt="demo">
          </a>

          <!-- main menu -->
          <div class="uk-flex uk-flex-right">
            <div class="uk-hidden-small">
              <nav class="tm-navbar uk-navbar tm-navbar-transparent">
                <div class="uk-container uk-container-center">
                  <ul class="uk-navbar-nav uk-hidden-small">
{{--
                    <!-- home menu  -->
                    <li class="uk-parent uk-active" data-uk-dropdown><a href="index.html">Home</a>

                    </li>

                    <!-- theme menu -->
                    <li class="uk-parent" data-uk-dropdown><a href="#">Exchange Information</a>
                      <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-1">
                        <div class="uk-grid uk-dropdown-grid">
                          <div class="uk-width-1-1">
                            <ul class="uk-nav uk-nav-navbar">
                              <li><a href="module-layouts.html">Indonesian</a></li>
                              <li class="uk-parent"><a href="#">Korean</a></li>

                            </ul>
                          </div>
                        </div>
                      </div>
                    </li>

                    <!-- Pages menu -->
                    <li class="uk-parent" data-uk-dropdown><a href="#">About Us</a>

                    </li>

                    <!-- Timetable menu -->
                    <li><a href="timetable.html">Trade</a></li>

                    <!-- News menu -->
                    <li class="uk-parent" data-uk-dropdown><a href="news.html">News</a>

                    </li>

                    <!-- Shop menu -->
                    <li class="uk-parent" data-uk-dropdown><a href="shop-grid.html">Contact Us</a>

                    </li> --}}
                  </ul>
                </div>
              </nav>
            </div>

            <!-- offcanvas nav icon -->
            <a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

            <!-- search button -->
            {{-- <div class="uk-navbar-content tm-navbar-more uk-visible-large uk-margin-left" data-uk-dropdown="{mode:'click'}">
              <a class="uk-link-reset"></a>
              <div class="uk-dropdown uk-dropdown-flip">
                <form action="#" class="uk-search" data-uk-search="" id="search-page" method="post" name="search-box">
                  <input class="uk-search-field" name="searchword" placeholder="search..." type="text"> <input name="task" type="hidden" value="search">
                  <input name="option" type="hidden" value=""> <input name="Itemid" type="hidden" value="502">
                </form>
              </div>
            </div> --}}
          </div>

        </div>
      </div>
    </div>
<!-- top-a -->
    <div class="tm-block-top-a uk-block uk-block-default" id="tm-top-a">
      <div class="uk-container uk-container-center">
        <section class="tm-top-a uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
          @yield('content')
        </section>
      </div>
    </div>
    <!-- headerbar -->
    <div class="tm-headerbar-container">
      <div class="uk-container uk-container-center">
        <div class="tm-headerbar">
          <div class="">
            <div class="uk-grid" data-uk-grid-margin="">
              <div class="uk-width-medium-1-4 uk-width-small-1-2">
                <div class="uk-flex">
                  <div class="tm-block-icon uk-icon-comments-o"></div>
                  <div class="tm-block-content">
                    <h3>Talk to us now</h3>
                    <p>+49 555 000 000</p>
                  </div>
                </div>
              </div>
              <div class="uk-width-medium-1-4 uk-width-small-1-2">
                <div class="uk-flex">
                  <div class="tm-block-icon uk-icon-map-signs"></div>
                  <div class="tm-block-content">
                    <h3>Visit our offices</h3>
                    <p>Mirage Plaza - Hamburg</p>
                  </div>
                </div>
              </div>
              <div class="uk-width-medium-1-4 uk-width-small-1-2">
                <div class="uk-flex">
                  <div class="tm-block-icon uk-icon-envelope-o"></div>
                  <div class="tm-block-content">
                    <h3>Send us an email</h3>
                    <p>hello@tadacoin.com</p>
                  </div>
                </div>
              </div>
              <div class="uk-width-medium-1-4 uk-width-small-1-2">
                <div class="uk-flex">
                  <div class="tm-block-icon uk-icon-clock-o"></div>
                  <div class="tm-block-content">
                    <h3>We are open all day</h3>
                    <p>8:00 am to 10:00 pm daily</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>




    <!-- bottom-b -->
    <div class="tm-block-bottom-b uk-block uk-block-default tm-block-fullwidth tm-grid-collapse" id="tm-bottom-b">
      <div class="uk-container uk-container-center">
        <section class="tm-bottom-b uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
          <div class="uk-width-1-1">
            <div class="uk-panel uk-contrast tm-inner-padding-large tm-overlay-secondary">

              <!-- background cover -->
              <div class="tm-background-cover uk-cover-background uk-flex uk-flex-center uk-flex-middle" data-uk-parallax="{bg: '-200'}" style="background-position: 50% 0px; background-image:url(images/background/bg-image-1.jpg)">
                <div class="uk-position-relative uk-container tm-inner-container">
                  <div class="uk-grid" data-uk-grid-margin="">

                    <div class="uk-width-medium-1-2">
                      <div class="tm-rotate-text uk-flex">
                        <h2 class="uk-h1 uk-module-title tm-thin-font">Bitcoin Arbitrage</h2>
                        <h2 class="uk-h1"><span class="tm-word">Opportunities</span> <span class="tm-word">Exchange</span> <span class="tm-word">Passionate.</span> <span class="tm-word">Calculator</span></h2>
                      </div>
                      <p>Bitcoin arbitrage is the buying of bitcoins on an exchange where the price is very low and selling it at an exchange where the price is relatively higher</p>
                      <p>The prices of Bitcoin vary on various exchanges, due to the fact that the markets are not directly linked, and the trading volume,&nbsp;on many exchanges, is low enough that the price does not adjust to the average right away.</p><br>
                      <a class="uk-button-line uk-button" href="#" target="_self">Learn more</a>
                    </div>

                    <div class="uk-width-medium-1-2 uk-flex uk-flex-center uk-flex-middle">
                      <div data-uk-scrollspy="{cls:'uk-animation-slide-left', delay:100}"><img alt="demo" height="355" src="images/demo/default/content/coins.png" width="512"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>

    <!-- bottom-c -->
    <div class="tm-block-bottom-c uk-block uk-block-primary" id="tm-bottom-c">
      <div class="uk-container uk-container-center">
        <section class="tm-bottom-c uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
          <div class="uk-width-1-1">
            <div class="uk-panel">
              <div class="uk-grid" data-uk-grid-margin="">
                <div class="uk-width-medium-1-2">
                  <h3 class="uk-margin-small-top">Sign up for our newsletter to get the latest news</h3>
                </div>
                <div class="uk-width-medium-1-2">

                  <!-- subscription form -->
                  <form id="subscribe_form" class="uk-form">
                  <div id="alert-msg-subscribe" class="alert-msg"></div>
                  <div class="uk-grid uk-grid-small" data-uk-grid-margin>

                    <div class="uk-width-medium-2-5">
                      <div><input type="text" placeholder="Your name" name="subscribe-name" class="uk-width-1-1" required="required"></div>
                    </div>

                    <div class="uk-width-medium-2-5">
                      <div><input type="email" placeholder="Email address" name="subscribe-email" class="uk-width-1-1" required="required"></div>
                    </div>

                    <div class="uk-width-medium-1-5">
                      <div class="form-group uk-margin-remove">
                        <button id="subscribe_button" type="submit" class="uk-button uk-button-default">Subscribe</button>
                      </div>
                    </div>

                  </div>
                </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>

    <!-- bottom-d -->
    <div class="tm-block-bottom-d uk-block uk-block-secondary tm-overlay-6" id="tm-bottom-d">
      <div class="uk-container uk-container-center">
        <section class="tm-bottom-d uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">

          <!-- block -->
          <div class="uk-width-1-1 uk-width-medium-1-4">
            <div class="uk-panel">
              <img alt="logo" height="40" src="images/demo/general/logo-white.png" width="140">
              <p>Tadacoin PTE LTD<br>
              Jakarta<br>
              Jl. Wijaya I<br>
              Jakarta Selatan<br>
              <br>
              +62218356124<br>
              hello@tadacoin.com<br>
              support@tadacoin.com</p>
            </div>
          </div>

          <!-- block -->
          <div class="uk-width-1-1 uk-width-medium-1-4">
            <div class="uk-panel">
              <h3 class="uk-h3 uk-module-title uk-margin-bottom">Recent News</h3>
              <div class="tm-item-block">
                <ul>
                  <li>
                    <a href="news-item.html">80 Percent of the Total Bitcoin Supply Have Now Been Mined</a>
                    <div class="uk-margin-small-top">
                      This weekend marks a milestone for bitcoin as 80 percent of...
                    </div>
                  </li>

                </ul>
              </div>
            </div>
          </div>

          <!-- block -->
          <div class="uk-width-1-1 uk-width-medium-1-4">
            <div class="uk-panel">
              <h3 class="uk-h3 uk-module-title uk-margin-bottom">Quick Links</h3>
              <ul class="uk-list list-icons">
                <li><i class="uk-icon-angle-right"></i><a href="#" target="_blank">Bitcoin.co.id</a></li>
                <li><i class="uk-icon-angle-right"></i><a href="#" target="_blank">bithumb.com</a></li>
                <li><i class="uk-icon-angle-right"></i><a href="#" target="_blank">Bitcoin.co.id</a></li>
                <li><i class="uk-icon-angle-right"></i><a href="#" target="_blank">bithumb.com</a></li>
              </ul>
            </div>
          </div>

          <!-- block -->
          <div class="uk-width-1-1 uk-width-medium-1-4">
            <div class="uk-panel">
              <h3 class="uk-h3 uk-module-title uk-margin-bottom">About Tada Coin</h3>
              <p>TadaCoin is Indonesian - Korean Arbitrage Platform</p>
              <p>Where people can see the information between both countries and make trade easily</p>
            </div>
          </div>
        </section>
      </div>
    </div>

    <!-- bottom-e -->
    <div class="tm-block-bottom-e uk-block uk-block-secondary" id="tm-bottom-e">
      <div class="uk-container uk-container-center">
        <section class="tm-bottom-e uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
          <div class="uk-width-1-1 uk-width-medium-1-2">
            <div class="uk-panel">
              <p>Copyright &copy; tadacoin 2018</p>
            </div>
          </div>
          <div class="uk-width-1-1 uk-width-medium-1-2">
            <div class="uk-panel">
              <div class="uk-text-right">
                <a class="uk-icon-button uk-icon-facebook" href="#" target="_blank"></a>
                <a class="uk-icon-button uk-icon-instagram" href="#" target="_blank"></a>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>

    <!-- modal log-in -->
    <div class="uk-modal" id="modal-a">
      <div class="uk-modal-dialog uk-panel-box uk-panel uk-panel-header uk-modal-dialog-small">
        <a class="uk-modal-close uk-close"></a>
        <div class="">
          <h3 class="uk-panel-title">Sign In</h3>
          <form action="#" class="uk-form" method="post">
            <div class="uk-form-row">
              <input class="uk-width-1-1" name="username" placeholder="Username" size="18" type="text">
            </div>
            <div class="uk-form-row">
              <input class="uk-width-1-1" name="password" placeholder="Password" size="18" type="password">
            </div>
            <div class="uk-form-row">
              <label for="remember-me">Remember Me</label> <input checked id="remember-me" name="remember" type="checkbox" value="yes">
            </div>
            <div class="uk-form-row">
              <button class="uk-button uk-button-primary" name="Submit" type="submit" value="Log in">Log in</button>
            </div>
            <ul class="uk-list uk-margin-bottom-remove">
              <li><a href="">Forgot your password?</a></li>
              <li><a href="">Forgot your username?</a></li>
              <li><a href="">Create an account</a></li>
            </ul>

          </form>
        </div>
      </div>
    </div>

    <!-- offcanvas menu -->
    <div id="offcanvas" class="uk-offcanvas">
      <div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
        <ul class="uk-nav uk-nav-parent-icon uk-nav-offcanvas" data-uk-nav>

          <!-- Home menu -->
          <li class="uk-parent uk-active"><a href="index.html">Home</a>
            <ul class="uk-nav-sub">
              <li><a href="index.html">Business Layout</a></li>
              <li><a href="boxed-layout.html">Boxed Layout</a></li>
              <li><a href="corporate-layout.html">Corporate Layout</a></li>
              <li><a href="one-page-layout.html">One Page Layout</a></li>
              <li><a href="minimal-layout.html">Minimal Layout</a></li>
            </ul>
          </li>

          <!-- Theme menu -->
          <li class="uk-parent"><a href="#">Theme</a>
            <ul class="uk-nav-sub">
              <li><a href="module-layouts.html">Module Layouts</a></li>
              <li class="uk-parent"><a href="#">Header Layouts</a>
                <ul>
                  <li><a href="header-default.html">Header Default</a></li>
                  <li><a href="header-center.html">Header Center</a></li>
                  <li><a href="header-right.html">Header Right</a></li>
                  <li><a href="header-full-right.html">Header Full Fight</a></li>
                </ul>
              </li>
              <li><a href="uikit-elements.html">UIkit Elements</a></li>
              <li><a href="color-styles.html">Color Styles</a></li>
              <li><a href="rtl-language-support.html">RTL Language Support</a></li>
            </ul>
          </li>

          <!-- Pages menu -->
          <li class="uk-parent"><a href="#">Pages</a>
            <ul class="uk-nav-sub">
              <li><a href="about-us.html">About Us</a></li>
              <li class="uk-parent"><a href="#">Our Team</a>
                <ul>
                  <li><a href="our-team-basic.html">Our team - Basic</a></li>
                  <li><a href="our-team-extended.html">Our team - Extended</a></li>
                  <li><a href="our-team-filter.html">Our team - Filter</a></li>
                </ul>
              </li>
              <li><a href="our-history.html">Our History</a></li>
              <li><a href="our-philosophy.html">Our Philosophy</a></li>
              <li><a href="our-services.html">Our Services</a></li>
              <li><a href="our-pricing.html">Our Pricing</a></li>
              <li><a href="our-rates.html">Our Rates</a></li>
              <li class="uk-parent"><a href="#">Gallery</a>
                <ul>
                  <li class="uk-parent"><a href="#">Single Portfolio</a>
                    <ul>
                      <li><a href="half-image.html">Half Image</a></li>
                      <li><a href="half-image-slider.html">Half Image Slider</a></li>
                      <li><a href="full-image.html">Full Image</a></li>
                      <li><a href="full-image-slider.html">Full Image Slider</a></li>
                    </ul>
                  </li>
                  <li class="uk-parent"><a href="#">Gallery Layout</a>
                    <ul>
                      <li><a href="2-column-gallery.html">2 Column Gallery</a></li>
                      <li><a href="3-column-gallery.html">3 Column Gallery</a></li>
                      <li><a href="4-column-gallery.html">4 Column Gallery</a></li>
                    </ul>
                  </li>
                  <li class="uk-parent"><a href="#">Grid Layout</a>
                    <ul>
                      <li><a href="2-column-grid.html">2 Column Grid</a></li>
                      <li><a href="3-column-grid.html">3 Column Grid</a></li>
                      <li><a href="4-column-grid.html">4 Column Grid</a></li>
                    </ul>
                  </li>
                  <li><a href="portfolio-blocks.html">Portfolio Blocks</a></li>
                  <li><a href="masonry-layout.html">Masonry Layout</a></li>
                  <li><a href="no-margin-layout.html">No margin Layout</a></li>
                </ul>
              </li>
              <li><a href="testimonials.html">Testimonials</a></li>
              <li><a href="faqs.html">FAQs</a></li>
              <li><a href="404.html">404 Error page</a></li>
              <li><a href="contact-us.html">Contact Us</a></li>
            </ul>
          </li>

          <!-- Timetable menu -->
          <li><a href="timetable.html">Timetable</a></li>

          <!-- Elements menu -->
          <li class="uk-parent"><a href="#">Elements</a>
            <ul class="uk-nav-sub">
              <li><a href="elements-typography.html">Typography</a></li>
              <li><a href="elements-column-blocks.html">Column Blocks</a></li>
              <li><a href="elements-buttons.html">Buttons</a></li>
              <li><a href="elements-dropdowns.html">Dropdowns</a></li>
              <li><a href="elements-alerts.html">Alerts &amp; Badges</a></li>
              <li><a href="elements-panels.html">Panels</a></li>
              <li><a href="elements-accordion.html">Accordion &amp; Toggles</a></li>
              <li><a href="elements-tooltips.html">Tooltips</a></li>
              <li><a href="elements-progress.html">Progress Bar</a></li>
              <li><a href="elements-tabs.html">Tabs</a></li>
              <li><a href="elements-tables.html">Tables</a></li>
              <li><a href="elements-images.html">Images</a></li>
              <li><a href="elements-modal-box.html">Modal Box</a></li>
              <li><a href="elements-lightbox.html">Lightbox</a></li>
              <li><a href="elements-slideshow.html">Slideshow</a></li>
              <li><a href="elements-dynamic-grid.html">Dynamic Grid</a></li>
              <li><a href="elements-animation.html">Animation</a></li>
              <li><a href="elements-counter.html">Counter</a></li>
              <li><a href="elements-slider.html">Slider</a></li>
              <li><a href="elements-parallax.html">Parallax</a></li>
              <li><a href="elements-icons.html">Icons</a></li>
              <li><a href="elements-cards.html">Cards</a></li>
              <li><a href="elements-audio-video.html">Audio/Video</a></li>
              <li><a href="elements-timeline.html">Timeline</a></li>
            </ul>
          </li>

          <!-- News menu -->
          <li class="uk-parent"><a href="news.html">News</a>
            <ul class="uk-nav-sub">
              <li><a href="news.html">News Category</a></li>
              <li><a href="news-item.html">News Item</a></li>
            </ul>
          </li>

          <!-- Shop menu -->
          <li class="uk-parent"><a href="shop-grid.html">Shop</a>
            <ul class="uk-nav-sub">
              <li><a href="shop-grid.html">Shop Grid</a></li>
              <li><a href="shop-cart.html">Shop Cart</a></li>
              <li><a href="shop-checkout.html">Shop Checkout</a></li>
              <li><a href="shop-single-product.html">Shop Single Product</a></li>
            </ul>
          </li>

        </ul>
      </div>
    </div>

  </div>

  <!-- jquery -->
  <script src="js/jquery/jquery.min.js" type="text/javascript"></script>

  <!-- uikit -->
  <script src="vendor/uikit/js/uikit.min.js" type="text/javascript"></script>
  <script src="vendor/uikit/js/components/accordion.min.js" type="text/javascript"></script>
  <script src="vendor/uikit/js/components/autocomplete.min.js" type="text/javascript"></script>
  <script src="vendor/uikit/js/components/datepicker.min.js" type="text/javascript"></script>
  <script src="vendor/uikit/js/components/grid.min.js" type="text/javascript"></script>
  <script src="vendor/uikit/js/components/lightbox.min.js" type="text/javascript"></script>
  <script src="vendor/uikit/js/components/parallax.min.js" type="text/javascript"></script>
  <script src="vendor/uikit/js/components/pagination.min.js" type="text/javascript"></script>
  <script src="vendor/uikit/js/components/sticky.min.js" type="text/javascript"></script>
  <script src="vendor/uikit/js/components/timepicker.min.js" type="text/javascript"></script>
  <script src="vendor/uikit/js/components/tooltip.min.js" type="text/javascript"></script>

  <!-- theme -->
  <script src="js/theme.js" type="text/javascript"></script>
  <script src="js/plyr.js" type="text/javascript"></script>
  @yield('custom_js')
</body>
</html>
