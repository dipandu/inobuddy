@extends('layouts.login')
@section('title', 'Find Agency & Tenders Online - DiPandu')

@section('custom_style')
<style>
  html, body{
    margin: 0 !important;
    padding: 0 !important;
    width: 100% !important;
    height: 100% !important;
    background-color: #f8f9fa !important;
  }

  .login-container{
    padding: 1% 20px;
    height: 100%;
  }

  .login-form-container{
    box-shadow: 0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12);
    border-radius: 3px;
    background: #fff;
    margin-top: 20px;
  }

  .login-form-header{
    padding: 25px 50px;
  }

  .login-form-logo{
    padding: 20px 10px;
    border-bottom: 1px solid #DEDEDE;
  }

  .login-form-body{
    padding: 15px 50px 35px 50px;
    border-bottom: 1px solid #DEDEDE;
  }

  .btn-social{
    font-weight: 400;
    text-align: center;
  }

  .hr-divider {
    margin: 25px 0;
    position: relative;
    z-index: 1;
    width: 100%;
    text-align: center;
  }

  .hr-divider-text {
    position: relative;
    z-index: 2;
    display: inline-block;
    margin-bottom: -10px;
    padding: 0 10px;
    background: #fff;
    font-weight: 700;
    font-size: 13px;
    color: #363f4d;
  }

  .hr-divider:before {
    position: absolute;
    top: 50%;
    left: 0;
    width: 100%;
    height: 1px;
    margin-top: -1px;
    border-top-style: solid;
    border-top-width: 1px;
    border-top-color: #DEDEDE;
    content: '';
    margin-top: 0;
  }

  input[type=checkbox]{
    margin-top: 2px;
  }

  button[type=submit]{
    margin-bottom: 30px;
  }

  .login-form-additional-list{
    float:left;
    width: 50%;
    display: relative;
    margin-bottom: 20px;
  }

  .login-form-additional{
    padding: 15px 0 45px 0;
    border-bottom: 1px solid #DEDEDE;
    font-size: 14px;
  }

  .login-form-footer{
    padding: 25px 0 10px 0;
    font-size: 14px;
  }
</style>
@endsection

@section('content')
  @if (!empty(Request::input('from')))
    @php
      $google_link = url('auth/google-login?from=').Request::input('from');
    @endphp
  @else
    @php
      $google_link = url('auth/google-login');
    @endphp
  @endif

  <div class="container" style="height: 100%;">
    <div class="row" style="height: 100%;">
      <div class="col-lg-5 col-md-8 col-sm-8 mx-auto login-container">
        <div class="login-form-container">
          <div>
            <div class="login-form-header text-center">
              <div class="login-form-logo">
                <h3><span style="color:#fed136;">INO</span>BUDDY</h3>
              </div>
            </div>
            <div class="login-form-body">
              <a href="{{$google_link}}" class="btn btn-block btn-social btn-google">
                <span class="fa fa-google"></span>
                Sign up with Google
              </a>
              <div class="hr-divider"><span class="hr-divider-text">OR</span></div>
              <form id="login_form">
                <div class="alert" style="display: none;">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="username" name="username" placeholder="Email or Username">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                  @if (!empty(Request::input('from')))
                    <input type="hidden" name="from" value="{{ Request::input('from') }}" />
                  @endif
                </div>
                <button type="submit" class="btn btn-primary w-100">Log In</button>
                <div class="login-form-additional">
                  <div class="login-form-additional-list">
                    <label><input type="checkbox" id="permanent_login" name="save_login"> Remember me</label>
                  </div>
                  <div class="login-form-additional-list text-right">
                    <a href="{{ URL::to('/forgot') }}">Forgot Password?</a>
                  </div>
                </div>
                <div class="login-form-footer text-center">
                  Don't have an account? <a href="{{ URL::to('signup') }}">Sign Up</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection



@section('custom_script')
<script>
$('#login_form').submit(function(e){
  e.preventDefault();

  $('#login_form :button[type=submit]').addClass('processing');
  $('#login_form :button[type=submit]').attr('disabled', true);
  var formData = new FormData($(this)[0]);

  $.ajax({
    type:"POST",
    url:'{{ URL::to('api/login') }}',
    data:formData,
    // dataType:"json",
    success:function(rsp){
      if(rsp.status){
        window.location = rsp.next_page;
      }else{
        $('.alert').show();
        $('.alert').addClass('alert-danger');
        var errorMessage = '';
        $.each(rsp.message, function(i, v){
          errorMessage+=v+'<br / />';
        });
        $('.alert').html(errorMessage);
        $('#login_form :button[type=submit]').attr('disabled', false);
        $('#login_form :button[type=submit]').removeClass('processing');
      }
    },
    statusCode: {
      404: function() {
        alert("Unable to fetch the tender! Please contact the administrator.");
        $(document.body).css({'cursor' : 'default'});
      }
    },
    cache: false,
    contentType: false,
    processData: false
  });
});
</script>
@endsection
