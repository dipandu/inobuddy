@extends('layouts.login')
@section('title', 'Find Agency & Tenders Online - DiPandu')

@section('custom_style')
<style>
  html, body{
    margin: 0 !important;
    padding: 0 !important;
    width: 100% !important;
    height: 100% !important;
    background-color: #f8f9fa !important;
  }

  .signup-container{
    padding: 1% 20px;
    height: 100%;
  }

  .signup-form-container{
    box-shadow: 0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12);
    border-radius: 3px;
    background: #fff;
    margin-top: 20px;
  }

  .signup-form-header{
    padding: 25px 50px;
  }

  .signup-form-logo{
    padding: 20px 10px;
    border-bottom: 1px solid #DEDEDE;
  }

  .signup-form-body{
    padding: 15px 50px 35px 50px;
    border-bottom: 1px solid #DEDEDE;
  }

  .btn-social{
    font-weight: 400;
    text-align: center;
  }

  .hr-divider {
    margin: 25px 0;
    position: relative;
    z-index: 1;
    width: 100%;
    text-align: center;
  }

  .hr-divider-text {
    position: relative;
    z-index: 2;
    display: inline-block;
    margin-bottom: -10px;
    padding: 0 10px;
    background: #fff;
    font-weight: 700;
    font-size: 13px;
    color: #363f4d;
  }

  .hr-divider:before {
    position: absolute;
    top: 50%;
    left: 0;
    width: 100%;
    height: 1px;
    margin-top: -1px;
    border-top-style: solid;
    border-top-width: 1px;
    border-top-color: #DEDEDE;
    content: '';
    margin-top: 0;
  }

  input[type=checkbox]{
    margin-top: 2px;
  }

  button[type=submit]{
    margin-bottom: 15px;
  }

  .signup-form-additional{
    padding: 15px 0 70px 0;
    border-bottom: 1px solid #DEDEDE;
    font-size: 14px;
  }

  .signup-form-additional-list{
    float:left;
    width: 50%;
    display: relative;
  }

  .signup-form-additional-list > p{
    font-size: 12px;
    padding-bottom: 20px;
  }

  .signup-form-footer{
    padding: 25px 0 10px 0;
    font-size: 14px;
  }
</style>
@endsection

@section('content')
  <div class="container" style="height: 100%;">
    <div class="row" style="height: 100%;">
      <div class="col-lg-5 col-md-8 col-sm-8 mx-auto signup-container">
        <div class="signup-form-container">
          <div>
            <div class="signup-form-header text-center">
              <div class="signup-form-logo">
                <h3><span style="color:#fed136;">INO</span>BUDDY</h3>
              </div>
            </div>
            <div class="signup-form-body">
              <a href="{{URL::to('/auth/google-login')}}" class="btn btn-block btn-social btn-google">
                <span class="fa fa-google"></span>
                Sign up with Google
              </a>
              <div class="hr-divider"><span class="hr-divider-text">OR</span></div>
              <form id="signup_form">
                <div class="alert" style="display: none;">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                  <aside class="error-pop-up" id="email_error"></aside>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
                  <aside class="error-pop-up" id="username_error"></aside>
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                  <aside class="error-pop-up" id="password_error"></aside>
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password" required>
                  <input type="hidden" name="_token" value="{{csrf_token()}}" />
                  <aside class="error-pop-up" id="password_confirmation_error"></aside>
                </div>
                <button type="submit" class="btn btn-primary w-100">Sign Up</button>
                <div class="signup-form-additional">
                  <div class="signup-form-additional-list w-100 text-justify">
                    <p>
                      By registering you confirm that you accept the Terms and Conditions and Privacy Policy
                    </p>
                  </div>
                </div>
                <div class="signup-form-footer text-center">
                  Already an Inobuddy.com member? <a href="<?= URL::to('login'); ?>">Log in</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('custom_script')
<script>
$('#email').change(function(e){
  $.ajax({
    type:'GET',
    url:'{{ URL::to('api/check-user-exist') }}?email='+$('#email').val(),
    dataType:'json',
    success:function(rsp){
      if(rsp === true){
        $('#email').removeClass('success-input');
        $('#email').addClass('error-input');
        $('#email_error').show();
        $('#email_error').html('This email address is already in use!');
      }else{
        $('#email').removeClass('error-input');
        $('#email').addClass('success-input');
        $('#email_error').hide();
      }
    },
    statusCode: {
      404: function() {
        alert('Unable to fetch the tender! Please contact the administrator.');
        $(document.body).css({'cursor' : 'default'});
      }
    },
    cache: false,
    contentType: false,
    processData: false
  });
});

$('#username').keyup(function(e){
  if($(this).val().length > 4){
    $.ajax({
      type:'GET',
      url:'{{ URL::to('api/check-user-exist') }}?username='+$('#username').val(),
      dataType:'json',
      success:function(rsp){
        if(rsp === true){
          $('#username').removeClass('success-input');
          $('#username').addClass('error-input');
          $('#username_error').show();
          $('#username_error').html('This username already exists, please choose another!');
        }else{
          $('#username').removeClass('error-input');
          $('#username').addClass('success-input');
          $('#username_error').hide();
        }
      },
      statusCode: {
        404: function() {
          alert('Unable to fetch the tender! Please contact the administrator.');
          $(document.body).css({'cursor' : 'default'});
        }
      },
      cache: false,
      contentType: false,
      processData: false
    });
  }
});

$('#password').change(function(e){
  if($(this).val().length < 8){
    $(this).removeClass('success-input');
    $(this).addClass('error-input');
    $('#password_error').show();
    $('#password_error').html('Password must be 8 characters minimum!');
    $(this).focus();
  }else{
    $(this).removeClass('error-input');
    $(this).addClass('success-input');
    $('#password_error').hide();
  }
});

$('#password_confirmation').change(function(e){
  if($(this).val() !== $('#password').val()){
    $(this).removeClass('success-input');
    $(this).addClass('error-input');
    $('#password_confirmation_error').show();
    $('#password_confirmation_error').html('Password do not match!');
    $(this).focus();
  }else{
    $(this).removeClass('error-input');
    $(this).addClass('success-input');
    $('#password_confirmation_error').hide();
  }
});

$('#signup_form').submit(function(e){
  e.preventDefault();

  $('#signup_form :button[type=submit]').addClass('processing');
  $('#signup_form :button[type=submit]').attr('disabled', true);
  var formData = new FormData($(this)[0]);
  $.ajax({
    type:'POST',
    url:'{{ URL::to('api/signup') }}',
    data:formData,
    dataType:'json',
    success:function(rsp){
      if(rsp.status){
        window.location = '{{URL::to('/profile')}}';
      }else{
        $('.alert').show();
        $('.alert').addClass('alert-danger');
        var errorMessage = '';
        $.each(rsp.message, function(i, v){
          errorMessage+=v+'<br / />';
        });
        $('.alert').html(errorMessage);
        $('#signup_form :button[type=submit]').attr('disabled', false);
        $('#signup_form :button[type=submit]').removeClass('processing');
      }
    },
    statusCode: {
      404: function() {
        alert('Unable to fetch the tender! Please contact the administrator.');
        $(document.body).css({'cursor' : 'default'});
      }
    },
    cache: false,
    contentType: false,
    processData: false
  });
});
</script>
@endsection
