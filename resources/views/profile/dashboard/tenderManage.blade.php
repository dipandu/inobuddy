@extends('layouts.master')
@section('meta_tag')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@if (session()->has('login_token'))
  @include('layouts.navbarMember')
@else
  @include('layouts.navbar')
@endif

@section('title', 'Find Agency & Tenders Online - DiPandu')

@section('custom_style')
<link href="https://portmeet.com/resource/css/dp_uploader.css" rel="stylesheet">
<style media="screen">
  .tender-category{
    padding: 10px 15px 10px 15px;
    background-color: #b3b3b3;
    color: #fff;
  }

  .file-storage{
    background-color: #e6e6e6;
    color: #000000;
  }
</style>
@endsection

@php
  if(!empty(request()->input('p'))){
    $offers_tab = '';
    $details_tab = '';
    $edit_tab = '';
    $offers_nav = '';
    $details_nav = '';
    $edit_nav = '';
    switch (request()->input('p')) {
      case 'offers':
        $offers_tab = 'show active';
        $offers_nav = 'active';
        break;
      case 'details':
        $details_tab = 'show active';
        $details_nav = 'active';
        break;
      case 'edit':
        $edit_tab = 'show active';
        $edit_nav = 'active';
        break;
      default:
        $offers_tab = 'show active';
        $offers_nav = 'active';
        break;
    }
  }else{
    $offers_tab = 'show active';
    $details_tab = '';
    $edit_tab = '';
    $offers_nav = 'active';
    $details_nav = '';
    $edit_nav = '';
  }

  // dd($tender_files);

  if(empty($tender_files)){
    $tender_files = [];
  }else{
    $tender_files = json_decode($tender_files);
  }
@endphp

@section('content')
<section class="dashboard-tender-section" style="padding-top: 7rem; background-color: #f0f0f0;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="section-heading text-uppercase">{{$tender_title}}</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12 table-responsive">
        <div class="card w-100" style="padding: 3rem 2rem 3rem 2rem;">
          <div class="row">
            <div class="col-12">
              <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link tabs-button {{$offers_nav}}" id="pills-home-tab" data-url="offers" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Offers</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tabs-button {{$details_nav}}" id="pills-profile-tab" data-url="details" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Tender Details</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tabs-button {{$edit_nav}}" id="pills-contact-tab" data-url="edit" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Edit</a>
                </li>
              </ul>
              <div class="tab-content pt-3" id="pills-tabContent">
                <div class="tab-pane fade {{$offers_tab}}" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                  <div class="row">
                    <div class="col-12">
                      @foreach ($tender_bids as $bid)
                      <div class="row">
                        <div class="col-2 col-md-2">
                          @if(file_exists(storage_path('app/public/'.$bid['user_nicename'].'/ppic/__profile_picture.png')))
                            <img src="{{url('storage/'.$bid['user_nicename'].'/ppic/__profile_picture.png')}}" id="profile_picture" class="img w-100" />
                          @else
                            <img src="https://cdn6.f-cdn.com/img/unknown.png" id="profile_picture" class="img w-100" />
                          @endif
                        </div>
                        <div class="col-10 col-md-7">
                          <h5>{{$bid['user_nicename']}}</h5>
                          <p class="d-sm-none d-md-block">
                            {{$bid['bid_description']}}
                          </p>
                        </div>
                        <div class="col-12 col-md-3">
                          <h5>{{$tender_currency}} {{$bid['bid_price']}}</h5>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          32 Reviews
                          <div class="row">
                            <div class="col-12">
                              99% Tenders Completed
                            </div>
                            <div class="col-12" style="margin-top: 10px;">
                              <button type="button" name="button" class="btn btn-success w-100">Hire</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <hr>
                      @endforeach
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade {{$details_tab}}" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                  <div class="row">
                    <div class="col-12 col-md-8 py-3">
                      <div class="row">
                        <div class="col-12 mb-2">
                          <h5>Tender Description</h5>
                          <hr />
                          <p>{{$tender_description}}</p>
                          <p style="font-size: 14px; color: #8e8e8e;">
                            Tender Deadline : {{$tender_deadline}}
                          </p>
                        </div>
                        <div class="col-12">
                          <h5>Business Category</h5>
                          <hr />
                          @php
                            $exploded_categories = explode(',', $tender_category);
                          @endphp
                          @foreach ($exploded_categories as $category)
                            <span class="tender-category mx-1 rounded">{{$category}}</span>
                          @endforeach
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-md-4 py-3">
                      <div class="row">
                        <div class="col-12 mb-2">
                          <h5>Tender Budget</h5>
                          <hr />
                          <h6>{{$tender_currency}} {{$tender_budget}}</h6>
                        </div>
                        <div class="col-12">
                          <h5>Attached Files</h5>
                          <hr />
                          <?php
                            if(!empty($tender_files)){
                              foreach ($tender_files as $key => $value) {
                          ?>
                          <a target="_blank" href="{{url('storage/'.$tender_creator.'/tenders/'.$id.'/'.$value)}}"><i class="fa fa-file-o"></i> {{$value}}</a><br />
                          <?php
                              }
                            }else{
                              echo "<span style='color: #a5a5a5;'>No files attached</span>";
                            }
                          ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade {{$edit_tab}}" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                  <form id="tender-edit-form" enctype="multipart/form-data" method="POST">
                    <div class="row tender-post-row">
                      <div class="col-12">
                        <div class="alert alert-danger d-none">

                        </div>
                      </div>
                    </div>
                    <div class="row tender-post-row">
                      <div class="col-md-12 col-sm-12 col-sm-offset-0">
                        <div class="form-group">
                          <label class="control-label tender-label" for="tender-name">Choose a name for your tender</label>
                          <input type="text" name="tender-name" required placeholder="e.g I need an refrigrator for my restaurant" id="tender-name" class="form-control" value="{{$tender_title}}">
                        </div>
                      </div>
                    </div>
                    <div class="row tender-post-row">
                      <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                          <label class="control-label tender-label" for="tender-description">Tell your buddy about your tender</label>
                          <p>Great tender descriptions include a little bit about yourself, details of what you are trying to achieve, and any decisions that you have already made about your tender If there are things you are unsure of, don't worry, a buddy will
                              be able to help you fill in the blanks. </p>
                          <textarea rows="5" name="tender-description" required placeholder="Describe your tender here..." id="tender-description" class="form-control">{{$tender_description}}</textarea>
                          <div class="w-100 float-left p-3 my-3 rounded file-storage">
                            <?php
                              if(!empty($tender_files)){
                                $file_no = 0;
                                $file_id = -1;
                                foreach ($tender_files as $key => $value) {
                                $file_no += 1;
                                $file_id += 1;
                            ?>
                            <div class="w-50 float-left mb-2" style="height: 38px; line-height: 38px;">
                              {{$file_no}}. <a target="_blank" style="color: #474747 !important;" href="{{url('storage/'.$tender_creator.'/tenders/'.$id.'/'.$value)}}"><i class="fa fa-file-o"></i> {{$value}}</a>
                            </div>
                            <div class="w-50 float-left mb-2">
                              <button type="button" class="btn btn-danger float-right file-delete" data-tender="{{$id}}" data-fileid="{{$file_id}}"><i class="fa fa-trash"></i></button>
                            </div>
                            <?php
                                }
                              }else{
                                echo "<span style='color: #a5a5a5;'>No files attached</span>";
                              }
                            ?>
                          </div>
                          <div id="tender-files-uploader" style="margin-top: 20px;">

                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row tender-post-row">
                      <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                          <label class="control-label tender-label" for="tender-category">What business category is your tender?</label>
                          <p>Enter up to 5 categories that best describe your tender Buddies will use these categories to find tender they are most interested and experienced in. </p>
                          <input type="text" name="tender-category" required placeholder="Business category" id="tender-category" class="form-control" value="{{$tender_category}}">
                        </div>
                      </div>
                    </div>
                    <div class="row tender-post-row">
                      <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                          <label class="control-label tender-label tender-budget-label" for="tender-deadline">Tender deadline</label>
                          <input type="text" name="tender-deadline" placeholder="Tender deadline" id="tender-deadline" class="form-control date" value="{{$tender_deadline}}">
                        </div>
                      </div>
                    </div>
                    <div class="row tender-post-row">
                      <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                          <label class="control-label tender-label tender-budget-label" for="tender-budget">What is your estimated budget?</label>
                          <select class="form-control tender-currency" name="tender-currency" required>
                            <option value="USD" selected>USD</option>
                            @php
                              $currency_value = array('AUD', 'GBP', 'IDR', 'JPY', 'KRW', 'MYR');
                            @endphp

                            @foreach ($currency_value as $cry)
                              @if ($tender_currency == $cry)
                                <option value="{{$cry}}" selected>{{$cry}}</option>
                              @else
                                <option value="{{$cry}}">{{$cry}}</option>
                              @endif
                            @endforeach
                          </select>
                          <input type="text" placeholder="0" class="form-control tender-budget" rules="number-only" name="tender-budget" required value="{{$tender_budget}}"  />
                        </div>
                      </div>
                    </div>
                    <div class="row tender-post-row">
                      <div class="col-md-12 col-sm-12">
                        <button class="btn btn-primary btn-lg" id="form-post-submit" type="submit">Save</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('custom_script')
<script src="https://portmeet.com/resource/js/dp_uploader.js"></script>
<script type="text/javascript">
  $(document).ready(function(e){
    $('#tender-category').tagsinput({
      maxTags: 7
    });
    $('#tender-files-uploader').dp_uploader({
      name:'tender-files[]',
      max_size:5000000,
      max_files:eval(5 - <?= count($tender_files); ?>),
      allowed_types:['document', 'image'],
      type:'multiple'
    });

    $('input[name=tender-deadline]').datepicker({
      format: 'yyyy-mm-dd',
      startDate: '+0d'
    });
  });

  $('.tabs-button').click(function(e){
    location.replace('{{url()->current().'?p='}}'+$(this).data('url'));
  });

  $('.file-delete').click(function(e){
    swal('Are you sure to delete the file?', {
      buttons: {
        cancel: 'Cancel',
        yes: true
      },
      icon: "warning",
      dangerMode: true
    })
    .then((value) => {
      if(value === 'yes'){
        $.ajax({
          type:'POST',
          url:'{{ URL::to('api/tender/delete-tender-file') }}',
          data:JSON.stringify({
            'file':$(this).data('fileid'),
            'tender':$(this).data('tender'),
            '_token':'{{csrf_token()}}'
          }),
          dataType:'json',
          success:function(rsp){
            console.log(rsp);
            if(rsp.status){
              location.reload();
            }else{
              $('.alert-danger').show();
              var errorMessage = '';
              $.each(rsp.message, function(i, v){
                errorMessage+=v+'<br / />';
              });
              $('.alert-danger').html(errorMessage);
            }
          },
          statusCode: {
            404: function() {
              alert("Unable to fetch the tender! Please contact the administrator.");
              $(document.body).css({'cursor' : 'default'});
            }
          },
          cache: false,
          contentType: 'application/json',
          processData: false
        });
      }
    });
  });

  $('#tender-edit-form').submit(function(e){
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    formData.append('_token', '{{csrf_token()}}');
    formData.append('tender_id', '{{$id}}');
    $.ajax({
      type:'POST',
      url:'{{ URL::to('api/tender/update-tender') }}',
      data:formData,
      success:function(rsp){
        if(rsp.status){
          location.reload();
        }else{
          $('.alert-danger').show();
          var errorMessage = '';
          $.each(rsp.message, function(i, v){
            errorMessage+=v+'<br / />';
          });
          $('.alert-danger').html(errorMessage);
        }
      },
      statusCode: {
        404: function() {
          alert("Unable to fetch the tender! Please contact the administrator.");
          $(document.body).css({'cursor' : 'default'});
        }
      },
      cache: false,
      contentType: false,
      processData: false
    });
  });
</script>
@endsection
