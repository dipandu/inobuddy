@extends('layouts.master')
@section('meta_tag')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@if (session()->has('login_token'))
  @include('layouts.navbarMember')
@else
  @include('layouts.navbar')
@endif

@section('title', 'Inobuddy : My Bids')

@section('custom_style')
  <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('content')

<section class="dashboard-tender-section" style="padding-top: 7rem; background-color: #f0f0f0;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="section-heading text-uppercase">My Bids</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12 table-responsive">
        <div class="card w-100" style="padding: 3rem 1rem 3rem 1rem;">
          <div class="row">
            <div class="col-12">
              <table class="table table-hover table-bordered">
                <thead>
                  <th>Tender Name</th>
                  <th class="text-center">Days to Deliver</th>
                  <th class="text-center">Price</th>
                  <th class="text-center">Status</th>
                  <th class="text-center">Action</th>
                </thead>
                <tbody>
                @foreach ($bids as $bid)
                    <tr>
                      <td><a href="{{url('tenders/'.$bid['tender_slug'])}}">{{$bid['tender_title']}}</a></td>
                      <td class="text-center">{{$bid['bid_days_to_deliver']}}</td>
                      <td class="text-center">{{$bid['tender_currency']}} {{$bid['bid_price']}}</td>
                      <td class="text-center">
                          @if ($bid['bid_status'] == 1)
                            <span class="badge badge-success">Awarded</span>
                          @elseif($bid['bid_status'] == 2)
                            <span class="badge badge-primary">Running</span>
                          @elseif($bid['bid_status'] == 3)
                            <span class="badge badge-danger">Rejected</span>
                          @elseif($bid['bid_status'] == 4)
                            <span class="badge badge-dark">Retracted</span>
                          @else
                            <span class="badge badge-secondary">No Response</span>  
                          @endif
                      </td>
                      <td class="text-center">
                        <select class="form-control action-selector" data-currency="{{$bid['tender_currency']}}" data-bid="{{$bid['id']}}">
                            <option disabled selected>Action</option>
                            @if (!in_array($bid['bid_status'], array(1,2,3,4)))
                            <option value="4">Retract Bid</option>
                            <option value="5">Edit</option>
                            @endif
                        </select>
                      </td>
                    </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="modal w-100" tabindex="-1" id="bidModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Place Bid</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="bidForm">
          <div class="row">
            <div class="col-12">
              <div class="alert alert-danger alert-dismissible fade show" style="display:none;" role="alert">
              </div>
            </div>
            <div class="col-12 mb-3">
              <div class="input-group w-100">
                <label class="float-left w-100">Your price</label>
                <div class="input-group-prepend">
                  <span class="input-group-text" id="bid_currency"></span>
                </div>
                <input name="price" id="price" type="text" rules="number-only" class="form-control" placeholder="0">
              </div>
            </div>
            <div class="col-12 mb-3">
              <div class="input-group w-25">
                <label>Days to deliver</label>
                <input name="days" id="days" type="text" class="form-control" rules="number-only" placeholder="1" value="1">
                <input type="hidden" name="ttb" id="ttb">
                <div class="input-group-append">
                  <span class="input-group-text">Days</span>
                </div>
              </div>
            </div>
            <div class="col-12">
              <label for="description">Description</label>
              <textarea name="description" id="description" placeholder="Description" rows="5" class="form-control"></textarea>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="placeBid" class="btn btn-primary">Place Bid</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_script')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

<script>
$(document).ready(function(){
  $('table').DataTable();
});

$('.action-selector').change(function(e){
  var bid = $(this).data('bid');
  var status = $(this).val();

  if(status !== '5'){
    $.ajax({
        type:"POST",
        url:'{{ URL::to('api/bids/change-status') }}',
        data:JSON.stringify({
        _token:'{{csrf_token()}}',
        'bid':bid,
        'status':status
        }),
        dataType:"json",
        success:function(rsp){
        if(rsp.status){
            swal({
                title: "Status Changed!",
                text: "This will close in 3 seconds.",
                icon: "success",
                timer: 3000
            }).then(function(e){
                location.reload();
            });
        }else{
            var errorMessage = '';
            $.each(rsp.message, function(i, v){
            errorMessage+=v+'<br / />';
            });
            swal({
                title: "Oopss..",
                content: errorMessage,
                icon: "error"
            }).then(function(e){
                location.reload();
            });
        }
        },
        statusCode: {
        404: function() {
            alert("Unable to fetch data! Please contact the administrator.");
            $(document.body).css({'cursor' : 'default'});
        }
        },
        cache: false,
        contentType: 'application/json',
        processData: false
    });  
  }else{
    $('#bidModal').modal('show');
  }
});

$('#bidModal').on('hide.bs.modal', function(e){
    location.reload();
});	
</script>
@endsection
