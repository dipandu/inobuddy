@extends('layouts.master')
@section('meta_tag')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@if (session()->has('login_token'))
  @include('layouts.navbarMember')
@else
  @include('layouts.navbar')
@endif

@section('title', 'Find Agency & Tenders Online - DiPandu')

@section('custom_style')
  <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('content')

<section class="dashboard-tender-section" style="padding-top: 7rem; background-color: #f0f0f0;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="section-heading text-uppercase">Tenders</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12 table-responsive">
        <div class="card w-100" style="padding: 3rem 1rem 3rem 1rem;">
          <div class="row">
            <div class="col-12">
              <table class="table table-hover table-bordered">
                <thead>
                  <th>Tender Name</th>
                  <th class="text-center">Offers</th>
                  <th class="text-center">Status</th>
                  <th class="text-center">Deadline</th>
                  <th class="text-center">Action</th>
                </thead>
                <tbody>
                  @foreach ($tenders as $tender)
                    <tr>
                      <td><a href="{{url('manage/tender/'.$tender->tender_slug)}}">{{$tender->tender_title}}</a></td>
                      <td class="text-center">0</td>
                      <td class="text-center">
                        @if ($tender->tender_status == '0')
                        <span class="badge badge-pill badge-danger">Suspended</span>
                        @elseif ($tender->tender_status == '1')
                        <span class="badge badge-pill badge-success">Active</span>
                        @endif
                      </td>
                      <td class="text-center">{{$tender->tender_deadline}}</td>
                      <td class="text-center">
                        <select data-tender="{{$tender->id}}" class="form-control action-selector">
                          <option selected disabled>Select Action</option>

                          @if ($tender->tender_status == '1')
                          <option value="1" selected disabled>Activate</option>
                          @else
                          <option value="1">Activate</option>
                          @endif

                          @if ($tender->tender_status == '0')
                          <option value="0" selected disabled>Suspend</option>
                          @else
                          <option value="0">Suspend</option>
                          @endif

                          @if ($tender->tender_status == '3')
                          <option value="3" selected disabled>Delete</option>
                          @else
                          <option value="3">Delete</option>
                          @endif
                        </select>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('custom_script')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

<script>
$(document).ready(function(){
  $('table').DataTable();
});

$('.action-selector').change(function(e){
  var tender = $(this).data('tender');
  var status = $(this).val();

  $.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:"POST",
    url:'{{ URL::to('api/tender/change-status') }}',
    data:JSON.stringify({
      _token:'{{csrf_token()}}',
      'tender':tender,
      'status':status
    }),
    dataType:"json",
    success:function(rsp){
      if(rsp.status){
        location.reload();
      }else{
        var errorMessage = '';
        $.each(rsp.message, function(i, v){
          errorMessage+=v+'<br / />';
        });
        alert(errorMessage);
        location.reload();
      }
    },
    statusCode: {
      404: function() {
        alert("Unable to fetch data! Please contact the administrator.");
        $(document.body).css({'cursor' : 'default'});
      }
    },
    cache: false,
    contentType: 'application/json',
    processData: false
  });
});
</script>
@endsection
