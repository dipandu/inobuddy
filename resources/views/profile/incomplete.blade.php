@extends('layouts.master')

@if (session()->has('login_token'))
  @include('layouts.navbarMember')
@else
  @include('layouts.navbar')
@endif

@section('title', 'Find Agency & Tenders Online - DiPandu')

@section('content')

<section id="profile-page-section" style="padding-top: 50px;">
  <div class="container">
    <div class="row">
      <div class="col-12 profile-page">
        <div class="card w-100">
          <div class="row">
            <div class="col-12 px-0">
              <div class="alert alert-warning">
                You didn't complete your profile yet. Please complete it first to use INOBUDDY!
              </div>
            </div>
            <div class="col-12 px-0">
              <div class="alert alert-danger" style="display:none;">
              </div>
            </div>
          </div>
          <form method="post" id="incomplete-profile-form" enctype="multipart/form-data">
            <div class="row mb-2">
              <div class="col-12 col-md-6 col-lg-3">
                @if(file_exists(storage_path('app/public/'.$user_nicename.'/ppic/__profile_picture.png')))
                  <img src="{{url('storage/'.$user_nicename.'/ppic/__profile_picture.png')}}" id="profile_picture" class="img w-100" />
                @else
                  <img src="https://cdn6.f-cdn.com/img/unknown.png" id="profile_picture" class="img w-100" />
                @endif
                <label for="profile_picture_uploader" class="btn btn-primary profile-picture-button"><i class="fa fa-pencil"></i></label>
                <input type="file" accept="image" class="profile_picture_uploader" id="profile_picture_uploader" name="profile_picture" hidden />
              </div>
              <div class="col-12 col-md-6 col-lg-9 pt-4">
                <div class="row">
                  <div class="form-group col-12">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" value="{{$user_nicename}}" disabled readonly required>
                  </div>
                  <div class="form-group col-12 col-sm-6">
                    <label for="organization_name">First name</label>
                    @if (!empty($user_first_name))
                      <input type="text" class="form-control" name="first_name" id="first_name" value="{{$user_first_name}}" placeholder="First name" required>
                    @else
                      <input type="text" class="form-control" name="first_name" id="first_name"  placeholder="First name" required>
                    @endif
                  </div>
                  <div class="form-group col-12 col-sm-6">
                    <label for="last_name">Last name</label>
                    @if (!empty($user_last_name))
                      <input type="text" class="form-control" name="last_name" id="last_name" value="{{$user_last_name}}" placeholder="Last name" required>
                    @else
                      <input type="text" class="form-control" name="last_name" id="last_name"  placeholder="Last name" required>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="row">
                  <div class="form-group col-12 col-lg-6">
                    <label for="country">Country</label>
                    <select class="form-control" name="country" id="country">
                      <option disabled selected>Select your country.</option>
                      @if (!empty($user_country))
                          @foreach (api_get('https://restcountries.eu/rest/v2/all') as $element)
                            @if ($user_country == $element->alpha2Code)
                              <option value="{{$element->alpha2Code}}" selected>{{$element->name}}</option>
                            @else
                              <option value="{{$element->alpha2Code}}">{{$element->name}}</option>
                            @endif
                          @endforeach
                      @else
                          @foreach (api_get('https://restcountries.eu/rest/v2/all') as $element)
                            <option value="{{$element->alpha2Code}}">{{$element->name}}</option>
                          @endforeach
                      @endif
                    </select>
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                  </div>
                  <div class="form-group col-12 col-lg-6">
                    <label for="phone">Phone Number</label>
                    @if (!empty($user_phone))
                      <input type="phone" class="form-control" id="phone" value="{{$user_phone}}" name="phone" required>
                    @else
                      <input type="phone" class="form-control" id="phone" placeholder="87763xxxx" name="phone" required>
                    @endif
                    <small id="emailHelp" class="form-text text-muted">Input your phone without the country code</small>
                  </div>
                  <div class="form-group col-12 col-lg-6">
                    <label for="phone">Organization Name</label>
                    @if (!empty($user_organization_name))
                      <input type="text" class="form-control" id="organization_name" value="{{$user_organization_name}}" placeholder="Organization name" name="organization_name" required>
                    @else
                      <input type="text" class="form-control" id="organization_name" placeholder="Organization name" name="organization_name" required>
                    @endif
                  </div>
                  <div class="form-group col-12 col-lg-6">
                    <label for="phone">Business Category</label>
                    @if (!empty($user_business_category))
                      <input type="text" class="form-control" id="business_category" value="{{$user_business_category}}" name="business_category">
                    @else
                      <input type="text" class="form-control" id="business_category" placeholder="Business Category" name="business_category">
                    @endif
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <button type="submit" class="btn btn-success w-100">
                      Save
                    </button>
                  </div>
                </div>
              </div>
            </div>
            {{-- <input type="submit" /> --}}
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('custom_script')
<script>
$(document).ready(function(){
  $('select[name=country]').select2();
  $('#business_category').tagsinput({
    maxTags: 7
  });
});


$('#email').change(function(e){
  $.ajax({
    type:'GET',
    url:'{{ URL::to('api/check-user-exist') }}?email='+$('#email').val(),
    dataType:'json',
    success:function(rsp){
      if(rsp === true){
        $('#email').removeClass('success-input');
        $('#email').addClass('error-input');
        $('#email_error').show();
        $('#email_error').html('This email address is already in use!');
      }else{
        $('#email').removeClass('error-input');
        $('#email').addClass('success-input');
        $('#email_error').hide();
      }
    },
    statusCode: {
      404: function() {
        alert('Unable to fetch the tender! Please contact the administrator.');
        $(document.body).css({'cursor' : 'default'});
      }
    },
    cache: false,
    contentType: false,
    processData: false
  });
});

var _URL = window.URL || window.webkitURL;
$('#profile_picture_uploader').change(function(){
  if(this.files[0]){
    var img = new Image();

    img.onload = function (e) {
      if((this.width - this.height) != 0){
        alert('ea kegedean wakkkakak');
        $('#profile_picture_uploader').val('');
        return;
      }else{
        $('#profile_picture').attr('src', e.path[0].src);
      }
    }

    img.src = _URL.createObjectURL(this.files[0]);
  }else{
    $('#profile_picture').attr('src', 'https://cdn6.f-cdn.com/img/unknown.png');
  }
});


$('#incomplete-profile-form').submit(function(e){
  e.preventDefault();
  $('#incomplete-profile-form :button[type=submit]').addClass('processing');
  $('#incomplete-profile-form :button[type=submit]').attr('disabled', true);

  var formData = new FormData($(this)[0]);

  $.ajax({
    type:"POST",
    url:'{{ URL::to('api/save-user-data') }}',
    data:formData,
    dataType:"json",
    success:function(rsp){
      console.log(rsp);
      if(rsp.status){
        location.reload();
      }else{
        $('.alert-danger').show();
        var errorMessage = '';
        $.each(rsp.message, function(i, v){
          errorMessage+=v+'<br / />';
        });
        $('.alert-danger').html(errorMessage);
        $('#incomplete-profile-form :button[type=submit]').attr('disabled', false);
        $('#incomplete-profile-form :button[type=submit]').removeClass('processing');
      }
    },
    statusCode: {
      404: function() {
        alert("Unable to fetch the tender! Please contact the administrator.");
        $(document.body).css({'cursor' : 'default'});
      }
    },
    cache: false,
    contentType: false,
    processData: false
  });
});
</script>
@endsection
