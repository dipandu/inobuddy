@extends('layouts.master')

@if (session()->has('login_token'))
  @include('layouts.navbarMember')
@else
  @include('layouts.navbar')
@endif

@if (!empty($user_organization_name))
  @section('title', 'Inobuddy : '.$user_organization_name)
@else
  @section('title', 'Inobuddy : User Profile')
@endif

@section('content')

@php
  $country = api_get('https://restcountries.eu/rest/v2/alpha/'.$user_country);
  $my_skills = explode(',', $user_business_category);
@endphp
<section class="profile-page-section" style="padding-top: 50px;">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-8 profile-page" style="margin-bottom: 25px;">
        <div class="card w-100">
          <div class="row">
            <div class="col-6">
              <img src="https://cdn6.f-cdn.com/img/unknown.png" class="img w-100" />
            </div>
            <div class="col-6">
              <h3 class="display-name">{{$user_nicename}}</h3>
              <p>
                @if(!empty($user_slogan))
                  {{$user_slogan}}
                @else
                  No slogan.
                @endif
              </p>
              <div class="row">
                <div class="col-12">
                  <i class="fa fa-briefcase"></i> {{$user_organization_name}}
                </div>
                <div class="col-12">
                  <i class="fa fa-globe"></i> <img src="{{$country->flag}}" alt="{{$country->name}}" class="img img-fluid" style="height: 15px; box-shadow: 0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12);"> {{$country->name}}
                </div>
                <div class="col-12">
                  <i class="fa fa-child"></i> Member since {{date('F d, Y')}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-12 col-sm-12 col-md-4 profile-page recommended-buddy-container" style="margin-bottom: 25px;">
        <div class="card w-100">
          <div class="row">
            <div class="col-12 recommended-buddy">
              <h4 class="mb-3">Recomended Parter</h4>
              <div class="row">
                <div class="col-6 mb-2 pr-2">
                  <div class="w-100">
                    <img src="https://cdn6.f-cdn.com/img/unknown.png" class="img w-100 h-100" />
                  </div>
                </div>
                <div class="col-6 mb-2 pl-2">
                  <div class="w-100">
                    <img src="https://cdn6.f-cdn.com/img/unknown.png" class="img w-100 h-100" />
                  </div>
                </div>
                <div class="col-6 mb-2 pr-2">
                  <div class="w-100">
                    <img src="https://cdn6.f-cdn.com/img/unknown.png" class="img w-100 h-100" />
                  </div>
                </div>
                <div class="col-6 mb-2 pl-2">
                  <div class="w-100">
                    <img src="https://cdn6.f-cdn.com/img/unknown.png" class="img w-100 h-100" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-12 col-sm-12 col-md-8 profile-page" style="margin-bottom: 25px;">
        <div class="card w-100">
          <div class="row">
            <div class="col-12 review-box">
              <h4 class="mb-3">Reviews</h4>
              <div class="row">
                <div class="col-12 mb-2">
                  <div class="row review-list">
                    <div class="col-4 col-sm-2">
                      <img src="https://cdn6.f-cdn.com/img/unknown.png" class="img img-fluid" />
                    </div>
                    <div class="col-6 col-sm-10">
                      <h4><a>Project Title</a></h4>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <p>
                        Review example for review list.
                      </p>
                    </div>
                  </div>
                  <div class="row review-list">
                    <div class="col-4 col-sm-2">
                      <img src="https://cdn6.f-cdn.com/img/unknown.png" class="img img-fluid" />
                    </div>
                    <div class="col-6 col-sm-10">
                      <h4><a>Project Title</a></h4>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <p>
                        Review example for review list.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>
@endsection
