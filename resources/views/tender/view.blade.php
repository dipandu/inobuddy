@extends('layouts.master')
@section('meta_tag')
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="title" content="{{$tender_title}}">
<meta name="description" content="{{substr($tender_description, 0, 150)}}...">
@endsection
@if (session()->has('login_token'))
  @include('layouts.navbarMember')
@else
  @include('layouts.navbar')
@endif

@section('title', 'Inobuddy : '.$tender_title)

@section('custom_style')
<link href="https://portmeet.com/resource/css/dp_uploader.css" rel="stylesheet">
<style media="screen">
  .tender-category{
    padding: 10px 15px 10px 15px;
    background-color: #b3b3b3;
    color: #fff;
  }

  .file-storage{
    background-color: #e6e6e6;
    color: #000000;
  }

  .tender-header-label{
    text-align: center;
  }

  .tender-header-label + h6{
    font-size: 1.4rem;
    text-align: center;
  }

  .table-listing{
    box-shadow: 0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12);
  }

  .table-listing tbody{
    border-top: none !important;
    border-left: 1px solid #f3f3f3;
    border-right: 1px solid #f3f3f3;
    border-bottom: 1px solid #f3f3f3;
    background-color: #fff;
  }

  .table-listing td{
    border-top: none !important;
  }

  .table-listing thead th {
    background-color: #474647;
    color: #fff;
    vertical-align: bottom;
    border-bottom: 0;
    border-top: 0;
    border-left: 1px solid #474647;
    border-right: 1px solid #474647;
    padding-top: 1rem;
    padding-bottom: 1rem;
    font-weight: 400;
  }
</style>
@endsection

@section('content')
<section class="dashboard-tender-section" style="padding-top: 7rem; background-color: #f0f0f0;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="section-heading text-uppercase">{{$tender_title}}</h2>
      </div>
    </div>
    <div class="row">
      {{-- Start of Mobile Tender Header Details --}}
      <div class="col-12 mb-3 d-block d-md-none">
        <div class="card w-100" style="padding: 3rem 2rem 3rem 2rem;">
          <div class="row">
            <div class="col-12">
              <div class="float-left px-4">
                <span class="tender-header-label">Bids</span>
                <h6>0</h6>
              </div>
              <div class="float-left px-4">
                <span class="tender-header-label">Tender Budget ({{$tender_currency}})</span>
                <h6>{{$tender_budget}}</h6>
              </div>
            </div>
          </div>
        </div>
      </div>
      {{-- End of Mobile Tender Header Details --}}


      {{-- Start of Desktop Tender Header Details --}}
      <div class="col-12 mb-4 d-none d-md-block">
        <div class="card w-100" style="padding: 3rem 2rem 3rem 2rem;">
          <div class="row">
            <div class="col-12">
              <div class="float-left px-4">
                <span class="tender-header-label">Bids</span>
                <h6>0</h6>
              </div>
              <div class="float-right px-4">
                <span class="tender-header-label ml-2">Status</span>
                @if ($tender_status == '1')
                  <h6 style="color: #0ff34a;">Active</h6>
                @else
                  <h6 style="color: #c01a1a;">Inactive</h6>
                @endif
              </div>
              <div class="float-left px-4">
                <span class="tender-header-label">Tender Budget ({{$tender_currency}})</span>
                <h6>{{$tender_budget}}</h6>
              </div>
            </div>
          </div>
        </div>
      </div>
      {{-- End of Desktop Tender Header Details --}}

      <div class="col-12">
        <div class="card w-100" style="padding: 3rem 2rem 3rem 2rem;">
          <div class="row">
            <div class="col-12 col-md-8 mb-3">
              <h5>Tender Description</h5>
              <p>{{$tender_description}}</p>
              <h5 class="py-3">Business Categories</h5>
              @php
                $exploded_categories = explode(',', $tender_category);
              @endphp
              @foreach ($exploded_categories as $category)
                <span class="tender-category mx-1 mb-2 float-left rounded">{{$category}}</span>
              @endforeach
            </div>   
            @if (session()->has('login_token'))
              @if ($tender_creator != decrypt(session()->get('uid')))
                 @if (empty($tender_bids[decrypt(session()->get('uid'))]))
                  @if ($tender_status == 1)
                  <div class="col-md-4">
                    <button type="button" class="btn btn-primary w-100 p-3" data-toggle="modal" data-target="#bidModal" name="button">Bid on This Tender</button>
                  </div>
                  @endif
                @endif    
              @endif
            @else
              <div class="col-md-4 d-none d-md-block">
                <a class="btn btn-primary w-100 p-3 text-white" href="{{url('login?from='.urlencode(request()->url()))}}">Bid on This Tender</a>
              </div>
            @endif
          </div>
        </div>
      </div>

      <div class="col-12 table-responsive mt-4">
        <table class="table table-listing table-hover">
          <thead>
            <th>Tender Bidder</th>
            <th>Days to Finish</th>
            {{-- <th>Rating</th> --}}
            <th>Bid ({{$tender_currency}})</th>
          </thead>
          <tbody>
            @if (!empty($tender_bids))
            @foreach ($tender_bids as $bid)
              <tr>
                <td>{{$bid['user_nicename']}}</td>
                <td>{{$bid['bid_days_to_deliver']}}</td>
                {{-- <td>Indonesia</td> --}}
                <td>{{$tender_currency}} {{$bid['bid_price']}}</td>
              </tr>
            @endforeach
            @else
            <tr>
              <td class="text-center" colspan="3"><b>No tender has no bids yet.</b></td>
            </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>

<div class="modal w-100" tabindex="-1" id="bidModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Place Bid</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="bidForm">
          <div class="row">
            <div class="col-12">
              <div class="alert alert-danger alert-dismissible fade show" style="display:none;" role="alert">
              </div>
            </div>
            <div class="col-12 mb-3">
              <div class="input-group w-100">
                <label class="float-left w-100">Your price</label>
                <div class="input-group-prepend">
                  <span class="input-group-text">{{$tender_currency}}</span>
                </div>
                <input name="price" id="price" type="text" rules="number-only" class="form-control" placeholder="0">
              </div>
            </div>
            <div class="col-12 mb-3">
              <div class="input-group w-25">
                <label>Days to deliver</label>
                <input name="days" id="days" type="text" class="form-control" rules="number-only" placeholder="1" value="1">
                <input type="hidden" name="ttb" id="ttb" value="{{$id}}">
                <div class="input-group-append">
                  <span class="input-group-text">Days</span>
                </div>
              </div>
            </div>
            <div class="col-12">
              <label for="description">Description</label>
              <textarea name="description" id="description" placeholder="Description" rows="5" class="form-control"></textarea>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="placeBid" class="btn btn-primary">Place Bid</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_script')
<script src="https://portmeet.com/resource/js/dp_uploader.js"></script>
<script type="text/javascript">
  $(document).ready(function(e){
    $('#placeBid').click(function(e){
      $(this).addClass('processing');
      $(this).attr('disabled', true);

      $.ajax({
        type:"POST",
        url:'{{url('api/bids/place-bid')}}',
        data:JSON.stringify({
          '_token':'{{csrf_token()}}',
          'price':$('#price').val(),
          'days':$('#days').val(),
          'description':$('#description').val(),
          'ttb':$('#ttb').val()
        }),
        dataType:"json",
        success:function(rsp){
          if(rsp.status){
            swal({
              title: "Bid Placed!",
              text: "This will close in 3 seconds.",
              icon: "success",
              timer: 3000
            }).then(function(e){
              location.reload();
            });
          }else{
            var errorMessage = '';
            $.each(rsp.messages, function(i, v){
              errorMessage+=v+'<br / />';
            });
            $('.alert').html(errorMessage);
            $('.alert').show();
            $('#placeBid').removeClass('processing');
            $('#placeBid').attr('disabled', false);
          }
        },
        statusCode: {
          404: function() {
            alert("Unable to fetch data! Please contact the administrator.");
            $(document.body).css({'cursor' : 'default'});
          }
        },
        cache: false,
        contentType: 'application/json',
        processData: false
      });
    });

    $('#bidModal').on('hide.bs.modal', function(e){

    });
  });
</script>
@endsection
