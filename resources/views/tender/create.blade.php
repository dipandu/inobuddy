@extends('layouts.master')

@if (session()->has('login_token'))
  @include('layouts.navbarMember')
@else
  @include('layouts.navbar')
@endif

@section('title', 'Inobuddy : Post Tender')

@section('custom_style')
<link href="https://portmeet.com/resource/css/dp_uploader.css" rel="stylesheet">
@endsection
@if (!empty(session()->get('tender_draft')))
  @php
    $tender_name = session()->get('tender_draft')['tender_name'];
    $tender_description = session()->get('tender_draft')['tender_description'];
    $tender_category = session()->get('tender_draft')['tender_category'];
    $tender_deadline = session()->get('tender_draft')['tender_deadline'];
    $tender_currency = session()->get('tender_draft')['tender_currency'];
    $tender_budget = session()->get('tender_draft')['tender_budget'];;
  @endphp
@else
  @php
    $tender_name = '';
    $tender_description = '';
    $tender_category = '';
    $tender_deadline = '';
    $tender_currency = '';
    $tender_budget = '';
  @endphp
@endif
@section('content')
<section class="tender-post-page-section" style="padding-top: 50px;">
  <div class="container">
    <div class="row tender-post-row">
      <div class="col-md-12">
        <h3 class="text-left">Tell us what you need done</h3>
        <p class="tender-create-p">Get free quotes from skilled freelancers within minutes, view profiles, ratings and portfolios and chat with them. Pay the freelancer only when you are 100% satisfied with their work. </p>
      </div>
    </div>
    @if(!empty(session()->get('errors')))
    <div class="row tender-post-row">
      <div class="col-md-12">
        <div class="alert alert-danger">
          {{-- @php
            print_r(session()->get('errors')->messages());
          @endphp --}}
        @foreach (session()->get('errors')->messages() as $error)
          <i style="width: 100%; display: block; font-style: normal;">{{$error[0]}}</i>
        @endforeach
        </div>
      </div>
    </div>
    @endif
    <form id="tender-post-form" method="post" action="{{url('save-tender')}}" enctype="multipart/form-data">
    <input type="hidden" value="{{csrf_token()}}" name="_token" />
    <div class="row tender-post-row">
      <div class="col-md-12 col-sm-12 col-sm-offset-0">
        <div class="form-group">
          <label class="control-label tender-label" for="tender-name">Choose a name for your tender</label>
          <input type="text" name="tender-name" required placeholder="e.g I need an refrigrator for my restaurant" id="tender-name" class="form-control" value="{{$tender_name}}">
        </div>
      </div>
    </div>
    <div class="row tender-post-row">
      <div class="col-md-12 col-sm-12">
        <div class="form-group">
          <label class="control-label tender-label" for="tender-description">Tell your buddy about your tender</label>
          <p>Great tender descriptions include a little bit about yourself, details of what you are trying to achieve, and any decisions that you have already made about your tender If there are things you are unsure of, don't worry, a buddy will
              be able to help you fill in the blanks. </p>
          <textarea rows="5" name="tender-description" required placeholder="Describe your tender here..." id="tender-description" class="form-control">{{$tender_description}}</textarea>
          <div id="tender-files-uploader" style="margin-top: 20px;">

          </div>
        </div>
      </div>
    </div>
    <div class="row tender-post-row">
      <div class="col-md-12 col-sm-12">
        <div class="form-group">
          <label class="control-label tender-label" for="tender-category">What business category is your tender?</label>
          <p>Enter up to 5 categories that best describe your tender Buddies will use these categories to find tender they are most interested and experienced in. </p>
          <input type="text" name="tender-category" required placeholder="Business category" id="tender-category" class="form-control" value="{{$tender_category}}">
        </div>
      </div>
    </div>
    <div class="row tender-post-row">
      <div class="col-md-12 col-sm-12">
        <div class="form-group">
          <label class="control-label tender-label tender-budget-label" for="tender-deadline">Tender deadline</label>
          <input type="text" name="tender-deadline" placeholder="Tender deadline" id="tender-deadline" class="form-control date" value="{{$tender_deadline}}">
        </div>
      </div>
    </div>
    <div class="row tender-post-row">
      <div class="col-md-12 col-sm-12">
        <div class="form-group">
          <label class="control-label tender-label tender-budget-label" for="tender-budget">What is your estimated budget?</label>
          <select class="form-control tender-currency" name="tender-currency" required>
            <option value="USD" selected>USD</option>
            @php
              $currency_value = array('AUD', 'GBP', 'IDR', 'JPY', 'KRW', 'MYR');
            @endphp

            @foreach ($currency_value as $cry)
              @if ($tender_currency == $cry)
                <option value="{{$cry}}" selected>{{$cry}}</option>
              @else
                <option value="{{$cry}}">{{$cry}}</option>
              @endif
            @endforeach
          </select>
          <input type="text" placeholder="0" class="form-control tender-budget" rules="number-only" name="tender-budget" required value="{{$tender_budget}}"  />
        </div>
      </div>
    </div>
    <div class="row tender-post-row">
      <div class="col-md-12 col-sm-12">
        <button class="btn btn-primary btn-lg" id="form-post-submit" type="submit">Post It!</button>
      </div>
    </div>
    </form>
    <div class="row tender-post-row">
      <div class="col-md-12 col-sm-12">
        <hr>
        <p class="tender-post-agreement">
          By clicking 'Post My Tender you agree to the Terms &amp; Conditions and Privacy Policy<br />
          If you decide to award your project we charge a 3% commission (minimum project fees apply).
        </p>
      </div>
    </div>
  </div>
</section>
@endsection

@section('custom_script')
<script src="https://portmeet.com/resource/js/dp_uploader.js"></script>
<script type="text/javascript">
  $(document).ready(function(e){
    $('#tender-category').tagsinput({
      maxTags: 7
    });

    $('#tender-files-uploader').dp_uploader({
      name:'tender-files[]',
      max_size:5000000,
      max_files:5,
      allowed_types:['document', 'image'],
      type:'multiple'
    });

    $('input[name=tender-deadline]').datepicker({
      format: 'yyyy-mm-dd',
      startDate: '+0d'
    });
  });
</script>
@endsection
