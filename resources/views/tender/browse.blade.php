@extends('layouts.master')

@if (session()->has('login_token'))
  @include('layouts.navbarMember')
@else
  @include('layouts.navbar')
@endif

@section('title', 'Inobuddy : Post Tender')

@section('custom_style')
<link href="https://portmeet.com/resource/css/dp_uploader.css" rel="stylesheet">
<style media="screen">
  .pagination{
    float: right;
    height: 100%;
    line-height: 36px;
  }

  .pagination > li{
    padding-right: 1rem;
    padding-left: 1rem;
  }

  .table-listing{
    box-shadow: 0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12);
  }

  .table-listing tbody{
    border-top: none !important;
    border-left: 1px solid #f3f3f3;
    border-right: 1px solid #f3f3f3;
    border-bottom: 1px solid #f3f3f3;
    background-color: #fff;
  }

  .table-listing td{
    border-top: none !important;
  }

  .table-listing thead th {
    background-color: #474647;
    color: #fff;
    vertical-align: bottom;
    border-bottom: 0;
    border-top: 0;
    border-left: 1px solid #474647;
    border-right: 1px solid #474647;
    padding-top: 1rem;
    padding-bottom: 1rem;
    font-weight: 400;
  }
</style>
@endsection
{{-- Pagination Preparation --}}
@php
  $url_param = '';
  $all_request = request()->input();
  $first_request = reset($all_request);
  $last_request= end($all_request);

  foreach ($all_request as $rk => $rv) {
    if($rk != 'page'){
      $url_param .= '&'.$rk.'='.$rv;
    }
  }

  $urlLimit = '';
  foreach ($all_request as $rklm => $rvlm) {
    switch ($rklm) {
      case 'page':
      #do nothing
        break;

      case 'limit':
      #do nothing
        break;

      default:
        $urlLimit .= '&'.$rklm.'='.$rvlm;
        break;
    }
  }

  $urlFilter = '';
  foreach ($all_request as $rkfl => $rvfl) {
    switch ($rkfl) {
      case 'page':
      #do nothing
        break;

      default:
        $urlFilter .= '&'.$rkfl.'='.$rvfl;
        break;
    }
  }

  $urlKeywords = '';
  foreach ($all_request as $rkky => $rvky) {
    switch ($rkky) {
      case 'page':
      #do nothing
        break;

      case 'q':
      #do nothing
        break;

      default:
        $urlKeywords .= '&'.$rkky.'='.$rvky;
        break;
    }
  }

  $urlPrice = '';
  foreach ($all_request as $rkpr => $rvpr) {
    switch ($rkpr) {
      case 'page':
      #do nothing
        break;

      case 'lowest_price':
      #do nothing
        break;

      case 'highest_price':
      #do nothing
        break;

      default:
        $urlPrice .= '&'.$rkpr.'='.$rvpr;
        break;
    }
  }

  $currentLimit = 10;
  if(request()->has('limit')){
    $currentLimit = request()->input('limit');
  }

  $lowest_price = $tenders['lowest_price'];
  $highest_price = $tenders['highest_price'];
  if(request()->has('lowest_price')){
    $lowest_price = request()->input('lowest_price');
  }

  if(request()->has('highest_price')){
    $highest_price = request()->input('highest_price');
  }
@endphp
@section('content')
<section class="px-1" style="background-color: #F0F0F0;">
  <div class="container">
    <form id="searchFilter">
      <div class="row mb-5">
        <div class="col-lg-12">
          <h3 class="section-heading text-uppercase">Find Tenders</h3>
          <hr />
        </div>
        <div class="col-12">
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="q" id="keywords" placeholder="Search by projects" value="{{request()->input('q')}}">
            <div class="input-group-append">
              <button class="btn btn-primary" id="searchButton" type="button"><i class="fa fa-search"></i> Search now</button>
            </div>
          </div>
        </div>
        <div class="col-12">
          <span class="float-left h-100 mr-2" style="padding-top: 13.3px;">Currency : </span>
          <select class="form-control float-left currency mr-3" style="width:95px; margin-top: 7.5px;" name="currency" id="currency">
            <option value="" selected>ALL</option>
            <option value="USD">USD</option>
            @php
              $currency_value = array('AUD', 'GBP', 'IDR', 'JPY', 'KRW', 'MYR');
            @endphp
            @foreach ($currency_value as $currency)
              @if (strtoupper(request()->input('currency')) == $currency)
                <option value="{{$currency}}" selected>{{$currency}}</option>
              @else
                <option value="{{$currency}}">{{$currency}}</option>
              @endif
            @endforeach
          </select>
          <p>
            <label for="amount">Price range:</label>
            <input type="text" id="amount" readonly style="font-weight:bold;">
          </p>
          <div id="slider-range"></div>
          <input type="hidden" name="lowest_price" id="lowest_price" value="{{$lowest_price}}" />
          <input type="hidden" name="highest_price" id="highest_price" value="{{$highest_price}}" />
        </div>
      </div>
      <div class="row mb-3">
          <div class="col-12">
            <select class="form-control float-right tenderLimit" style="width:75px; margin-top: 7.5px;" name="limit" id="limit">
              @php
                $limitation = array(10, 25, 50, 100);
              @endphp
              @foreach ($limitation as $lmt)
                @if ($lmt == request()->input('limit'))
                  <option value="{{$lmt}}" selected>{{$lmt}}</option>
                @else
                  <option value="{{$lmt}}">{{$lmt}}</option>
                @endif
              @endforeach
            </select>
            <span class="float-right h-100 mr-2" style="padding-top: 13.3px;">Tenders per page : </span>
          </div>
        </div>
    </form>
    <div class="row">
      <div class="col-12">
        <div class="row">
          <div class="col-12 table-responsive">
            <table class="table table-listing">
              <thead>
                <th style="width: 50%;">Tenders</th>
                <th style="width: 15%;" class="text-center">Offers</th>
                <th style="width: 15%;" class="text-center">Started</th>
                <th style="width: 20%;" class="text-right">Budget</th>
              </thead>
              <tbody>
                @empty ($tenders['tenders'])
                <tr>
                  <td colspan="4" class="text-center"><b>No tenders found</b></td>
                </tr>
                @endempty
                @foreach ($tenders['tenders'] as $tender)
                <tr>
                  <td>
                    <h5><a href="{{url('tenders/'.$tender['tender_slug'])}}">{{$tender['tender_title']}}</a></h5>
                    <p>
                      @php
                        $description_exp[$tender['id']] = explode(' ', $tender['tender_description']);
                        $description[$tender['id']] =  "";
                        $no[$tender['id']] = 0;
                        foreach ($description_exp[$tender['id']] as $desc) {
                          $no[$tender['id']] += 1;
                          if($no[$tender['id']] < 15){
                            $description[$tender['id']] .= $desc." ";
                          }
                        }

                        echo $description[$tender['id']];
                      @endphp
                    </p>
                    @php
                      $cats_exp[$tender['id']] = explode(',', $tender['tender_category']);
                      foreach ($cats_exp[$tender['id']] as $cat) {
                        if($cat != end($cats_exp[$tender['id']])){
                          echo '<a href="'.url('tenders?category='.$cat).'">'.$cat.'</a>, ';
                        }else{
                          echo '<a href="'.url('tenders?category='.$cat).'">'.$cat.'</a>';
                        }
                      }
                    @endphp
                  </td>
                  <td class="text-right">0</td>
                  <td class="text-right">{{substr($tender['created_at'], 0, 10)}}</td>
                  <td class="text-right">{{$tender['tender_currency']}} {{$tender['tender_budget']}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            Total data {{$tenders['paginator']->total()}}
            <ul class="pagination">
              @if ($tenders['currentPage'] > 1)
                <li><a href="{{url('tenders?page=1').$url_param}}"><i class="fa fa-step-backward"></i></a></li>
                <li><a href="{{url('tenders?page=').($tenders['currentPage'] - 1).$url_param}}"><i class="fa fa-chevron-left"></i></a></li>
              @endif

              @for ($i = max(1, $tenders['currentPage'] - 3); $i <= min($tenders['currentPage'] + 3, $tenders['lastPage']); $i++)
                @if ($i == $tenders['currentPage'])
                  <li class="active"><span>{{$i}}</span></li>
                @else
                  <li><a href="{{url('tenders?page=').$i.$url_param}}">{{$i}}</a></li>
                @endif
              @endfor

              @if ($tenders['currentPage'] < $tenders['lastPage'])
                <li><a href="{{url('tenders?page=').($tenders['currentPage'] + 1).$url_param}}"><i class="fa fa-chevron-right"></i></a></li>
                <li><a href="{{url('tenders?page=').$tenders['lastPage'].$url_param}}"><i class="fa fa-step-forward"></i></a></li>
              @endif
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection

@section('custom_script')
<script type="text/javascript">
  $(document).ready(function(e){
    $( "#slider-range" ).slider({
      range: true,
      min: {{$tenders['lowest_price']}},
      max: {{$tenders['highest_price']}},
      values: [ {{$lowest_price}}, {{$highest_price}} ],
      slide:  function(event, ui){
        $( "#amount" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
      },
      change: function(event, ui){
        $('#lowest_price').val(ui.values[0]);
        $('#highest_price').val(ui.values[1]);
        var replacedUrl = '{{url('tenders?')}}'+$('#searchFilter').serialize();
        window.location = replacedUrl.replace('&amp;','&');
      }
    });
    $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) + " - " + $( "#slider-range" ).slider( "values", 1 ) );
  });

  $('#searchButton').click(function(e){
    var replacedUrl = '{{url('tenders?')}}'+$('#searchFilter').serialize();
    window.location = replacedUrl.replace('&amp;','&');
  });

  $('select[name=currency]').change(function(e){
    $('#lowest_price').val('');
    $('#highest_price').val('');
  });

  $('input, select').change(function(e){
    var replacedUrl = '{{url('tenders?')}}'+$('#searchFilter').serialize();
    window.location = replacedUrl.replace('&amp;','&');
  });
</script>
@endsection
