<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\DB;
use Illuminate\Routing\UrlGenerator;
use Validator;
use App\Models\Users as Users;
use App\Models\Tenders as Tenders;
use App\Models\Tenders_meta as Tenders_meta;
use App\Models\Users_meta as Users_meta;
/**
 * User API Controller
 */
class TenderController extends Controller
{
  public function postTender()
  {
    return view('tender/create');
  }

  public function saveTender(Request $request)
  {
    $data_received = [
      'tender_name' => $request->input('tender-name'),
      'tender_description' => $request->input('tender-description'),
      'tender_category' => $request->input('tender-category'),
      'tender_deadline' => $request->input('tender-deadline'),
      'tender_currency' => $request->input('tender-currency'),
      'tender_budget' => $request->input('tender-budget')
    ];

    if(!session()->has('login_token')){
      session(['tender_draft' => $data_received]);
      return redirect(url('login?from='.urlencode(url('post-tender'))));
    }

    $validation_rules = [
      'tender-name' => 'required',
      'tender-description' => 'required|min:10',
      'tender-category' => 'required',
      'tender-deadline' => 'required',
      'tender-currency' => 'required',
      'tender-budget' => 'required|integer'
    ];

    if($request->hasFile('tender-files')){
      $file_validation_rules['tender-files'] = 'mimes:docx,doc,xlsx,xls,pdf,ppt,pptx,jpg,jpeg,bmp,gif,png';
      $file_validator = Validator::make($request->file('tender-files'), $file_validation_rules);

      if(!$file_validator->passes()){
        $tender_draft = [
          'tender_draft' => $data_received,
        ];
        session()->flash('errors', $file_validator->messages());
        session($tender_draft);
        return redirect(url('post-tender'));
      }
    }

    $validator = Validator::make($request->input(), $validation_rules);

    if($validator->passes()){
      $tender = new Tenders;
      $tender->tender_title = $request->input('tender-name');
      $tender->tender_creator = Users::where('user_nicename', decrypt(session()->get('login_token')))->first()->id;
      $tender->tender_description = $request->input('tender-description');
      $tender->tender_category = $request->input('tender-category');
      $tender->tender_deadline = $request->input('tender-deadline');
      $tender->tender_currency = $request->input('tender-currency');
      $tender->tender_budget = $request->input('tender-budget');
      $tender->save();
      // dd($tender->tender_creator);
      if($request->hasFile('tender-files')){
        foreach ($request->file('tender-files') as $file) {
          $tender_files[] = $file->getClientOriginalName();
          $move_file[] = $file->storeAs($tender->tender_creator.'/tenders/'.$tender->id, $file->getClientOriginalName(), 'public');
        }
        $tender_files = json_encode($tender_files);
      }else{
        $tender_files = json_encode([]);
      }
      $tender_slug = str_slug($tender->id.' '.$request->input('tender-name'), '-');
      $tender->where('id', $tender->id)->update([
        'tender_slug' => $tender_slug,
        'tender_files' => $tender_files
      ]);



      return redirect('dashboard/tenders');
    }else{
      $tender_draft = [
        'tender_draft' => $data_received,
      ];

      session()->flash('errors', $validator->messages());
      session($tender_draft);
      return redirect(url('post-tender'));
    }

  }

  public function updateTender(Request $request)
  {
    // dd($request->input('tender_id'));
    $data_received = [
      'tender_name' => $request->input('tender-name'),
      'tender_description' => $request->input('tender-description'),
      'tender_category' => $request->input('tender-category'),
      'tender_deadline' => $request->input('tender-deadline'),
      'tender_currency' => $request->input('tender-currency'),
      'tender_budget' => $request->input('tender-budget')
    ];

    if(!session()->has('login_token')){
      $result = [
        'status' => false,
        'message' => [
          0 => 'Failed to authenticate your login!'
        ]
      ];

      return $result;
    }

    $validation_rules = [
      'tender-name' => 'required',
      'tender-description' => 'required|min:10',
      'tender-category' => 'required',
      'tender-deadline' => 'required',
      'tender-currency' => 'required',
      'tender-budget' => 'required|integer'
    ];

    if($request->hasFile('tender-files')){
      $file_validation_rules['tender-files'] = 'mimes:docx,doc,xlsx,xls,pdf,ppt,pptx,jpg,jpeg,bmp,gif,png';
      $file_validator = Validator::make($request->file('tender-files'), $file_validation_rules);

      if(!$file_validator->passes()){
        $result = [
          'status' => false,
          'message' => $file_validator->messages()
        ];

        return $result;
      }
    }

    $validator = Validator::make($request->input(), $validation_rules);

    if($validator->passes()){
      $tender = Tenders::find($request->input('tender_id'));
      $tender->tender_title = $request->input('tender-name');
      $tender->tender_slug = str_slug($request->input('tender_id').' '.$request->input('tender-name'), '-');
      $tender->tender_description = $request->input('tender-description');
      $tender->tender_category = $request->input('tender-category');
      $tender->tender_deadline = $request->input('tender-deadline');
      $tender->tender_currency = $request->input('tender-currency');
      $tender->tender_budget = $request->input('tender-budget');
      if($request->hasFile('tender-files')){
        foreach ($request->file('tender-files') as $file) {
          $tender_files[] = $file->getClientOriginalName();
          $move_file[] = $file->storeAs($tender->tender_creator.'/tenders/'.$tender->id, $file->getClientOriginalName(), 'public');
        }

        if(!empty($tender->tender_files)){
          $tender->tender_files = json_encode(array_merge(json_decode($tender->tender_files), $tender_files));
        }else{
          $tender->tender_files = json_encode($tender_files);
        }
      }
      $tender->save();

      $result = [
        'status' => true
      ];

      return $result;
    }else{
      $result = [
        'status' => false,
        'message' => $validator->messages()
      ];

      return $result;
    }

  }

  public function changeStatus(Request $request)
  {
    if(!session()->has('login_token')){
      return abort(404);
    }

    $tender = Tenders::find($request->input('tender'));
    $tender->tender_status = $request->input('status');
    if($tender->save()){
      $result = [
        'status' => true
      ];
    }else{
      $result = [
        'status' => false,
        'message' => [
          0 => 'Failed to update the status'
        ]
      ];
    }
    return $result;
  }

  public function deleteFile(Request $request)
  {
    $user = new Users;
    $user_id = $user->get_userId(decrypt(session()->get('login_token')));

    $tender = new Tenders;
    $tender_data = $tender->get_tenderById($request->input('tender'));

    if($tender_data->tender_creator != $user_id){
      $result = [
        'status' => false,
        'message' => [
          'authentication' => 'Access prohibited!'
        ]
      ];
      return $result;
    }

    $tender_files = json_decode($tender_data->tender_files);

    if(file_exists(storage_path('app/public/'.$user_id.'/tenders/'.$tender_data->id.'/'.$tender_files[$request->input('file')]))){
      unlink(storage_path('app/public/'.$user_id.'/tenders/'.$tender_data->id.'/'.$tender_files[$request->input('file')]));
    }

    unset($tender_files[$request->input('file')]);

    if(!empty($tender_files)){
      foreach ($tender_files as $file) {
        $new_file[] = $file;
      }
    }else{
      $new_file = [];
    }

    $new_tender = Tenders::where('id', $request->input('tender'))
    ->update(['tender_files' => json_encode($new_file)]);

    $result = [
      'status' => true
    ];

    return $result;
  }

  public function browseTenders(Request $request)
  {
    if(!empty($request->input('limit'))){
      $limitPerPage = $request->input('limit');
    }else{
      $limitPerPage = 10;
    }

    $where = [];

    if($request->has('currency')){
      $where[0] = [
        'key' => 'currency',
        'value' => $request->input('currency')
      ];
    }

    if($request->has('highest_price') or $request->has('lowest_price')){
      $lowest_price = 0;
      $highest_price = 0;

      if($request->has('lowest_price')){
        $lowest_price = $request->input('lowest_price');
      }

      if($request->has('highest_price')){
        if($request->input('highest_price') < $lowest_price){
          $highest_price = $lowest_price;
        }else{
          $highest_price = $request->input('highest_price');
        }
      }

      $where[1] = [
        'key' => 'price_range',
        'value' => [$lowest_price, $highest_price]
      ];
    }

    if($request->has('q')){
      $where[2] = [
        'key' => 'query',
        'value' => $request->input('q')
      ];
    }

    $tenders_parent = new Tenders;
    $tender_data = $tenders_parent->get_browseTender($where, $limitPerPage);

    // dd($where);

    // dd($tender_data);
    // foreach ($tenders_parent as $tender_parent) {
    //   $tenders=
    // }
    return view('tender/browse', ['tenders' => $tender_data]);
  }

  public function viewTender($tender_slug)
  {
    if(empty($tender_slug)){
      abort(404);
    }

    $exploded_tender_slug = explode('-', $tender_slug);

    $tender_db = new Tenders;
    $tender_data = $tender_db->get_tenderById($exploded_tender_slug[0]);

    $tender_data['tender_bids'] = BidsController::getBidsByTender($exploded_tender_slug[0]);

    // dd($tender_data->toArray());

    return view('tender/view', $tender_data);
  }
}
