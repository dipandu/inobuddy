<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\DB;
use Illuminate\Routing\UrlGenerator;
use Validator;
use App\Models\Users as Users;
use App\Models\Users_meta as Users_meta;
/**
 * User API Controller
 */
class ProfileController extends Controller
{
  public function profile($username)
  {
    if(session()->has('login_token')){
      $login_token = decrypt(session()->get('login_token'));
    }else{
      $login_token = "";
    }

    if($username == $login_token){
      $user = Users::where('user_nicename', decrypt(session()->get('login_token')));

      // dd($user->first()->status);

      if ($user->first()->user_status == 0) {
        $meta = Users_meta::where('users_id', $user->first()->id);
        $user_data = $user->first()->toArray();
        $user_meta = $meta->get()->toArray();

        foreach ($user_meta as $key => $value) {
          $user_data[$value['meta_key']] = $value['meta_value'];
        }

        return view('profile/incomplete', $user_data);
      }else{
        $meta = Users_meta::where('users_id', $user->first()->id);
        $user_data = $user->first()->toArray();
        $user_meta = $meta->get()->toArray();

        foreach ($user_meta as $key => $value) {
          $user_data[$value['meta_key']] = $value['meta_value'];
        }

        return view('profile/member', $user_data);
      }
    }else{
      //If user suspended
      $user = Users::where('user_nicename', $username);
      if (!$user->first()) {
        return abort(404);
      }

      if($user->first()->status == '3'){
        //Show suspended page
        echo "suspended";
      }else{
        $meta = Users_meta::where('users_id', $user->first()->id);
        $user_data = $user->first()->toArray();
        $user_meta = $meta->get()->toArray();

        foreach ($user_meta as $key => $value) {
          $user_data[$value['meta_key']] = $value['meta_value'];
        }

        return view('profile/guest', $user_data);
      }
    }
  }

  public function check_user_exist(Request $request)
  {
    if(!empty($request->input('email'))){
      $user = Users::where('user_email', $request->input('email'))
      ->get();

      if (!$user->isEmpty()) {
        $result = 'true';
      }else{
        $result = 'false';
      }
    }else{
      $user = Users::where('user_nicename', $request->input('username'))
                      ->get();

      if (!$user->isEmpty()) {
        $result = 'true';
      }else{
        $result = 'false';
      }
    }

    return $result;

    // if (empty($user)) {
    //   $result = 'false';
    // }else{
    //   $result = 'true';
    // }
    //
    // return $request->input();
  }

  public function profileEditor()
  {
    if(!session()->has('login_token')){
      App::abort(404, 'Not Found');
    }

    $user = Users::where('user_nicename', decrypt(session()->get('login_token')));
    $meta = Users_meta::where('users_id', $user->first()->id);
    $user_data = $user->first()->toArray();
    $user_meta = $meta->get()->toArray();

    foreach ($user_meta as $key => $value) {
      $user_data[$value['meta_key']] = $value['meta_value'];
    }

    return view('profile/edit', $user_data);
  }
}
