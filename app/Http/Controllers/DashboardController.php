<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\DB;
use Illuminate\Routing\UrlGenerator;
use Validator;
use App\Models\Users as Users;
use App\Models\Tenders as Tenders;
use App\Models\Tenders_meta as Tenders_meta;
use App\Models\Users_meta as Users_meta;
/**
 * User API Controller
 */
class DashboardController extends Controller
{
  public function tenders(Request $request)
  {
    if(!session()->has('login_token')){
      return redirect('login?from='.urlencode(url('dashboard/tenders')));
    }

    $user_id = decrypt(session()->get('uid'));
    // $
    $tenders_db = new Tenders;
    // $tenders = $tenders_db::where('tender_creator', $user_id)
    // ->with('tender_meta')->get();

    $tenders = $tenders_db->get_user_tender($user_id);

    $data = ['tenders' => $tenders];
    return view('profile/dashboard/tenders', $data);
  }

  public function manageTender($tender_slug)
  {
    if(!session()->has('login_token')){
      return redirect('login?from='.urlencode(url()->current()));
    }

    $exploded_tender_slug = explode('-', $tender_slug);

    $user = new Users;
    $user_id = $user->get_userId(decrypt(session()->get('login_token')));

    $tender_id = $exploded_tender_slug[0];
    $tender = new Tenders;
    $tender_data = $tender->get_tenderById($tender_id);

    $tender_data['tender_bids'] = BidsController::getBidsByTender($tender_id);

    if($user_id != $tender_data['tender_creator']){
      abort(404);
    }

    if(empty($tender_data)){
      abort(404);
    }
    // dd($tender_data);

    return view('profile/dashboard/tenderManage', $tender_data);
  }

  public function bids()
  {
    if(!session()->has('login_token')){
      return redirect('login?from='.urlencode(url('dashboard/bids')));
    }

    $bids = BidsController::getBidsByUser(decrypt(session()->get('uid')));

    return view('profile/dashboard/bids', ['bids' => $bids]);
  }
}
