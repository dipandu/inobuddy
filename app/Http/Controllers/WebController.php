<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\DB;

// use App\Models\Currency;

class WebController extends Controller
{
    //
    public function index()
    {
      // $currency = Currency::find(1);
      // // dd($currency);
      // $results = [
      //   'IDR_KRW' => ($currency->idr_krw),
      //   'IDR_USD' => ($currency->idr_usd),
      //   'KRW_IDR' => ($currency->krw_idr),
      //   'KRW_USD' => ($currency->krw_usd),
      //   'USD_IDR' => ($currency->usd_idr),
      //   'USD_KRW' => ($currency->usd_krw),
      //   'last_update' => date('Y-m-d', $currency->updated_at->timestamp)
      // ];
      //
      // $c_date = date('Y-m-d');
      // if($c_date > substr($currency->updated_at, 0, 11)){
      //   $currency = json_decode(api_get('https://dipandu.net/get_currency'));
      //
      //   $results = [
      //     'IDR_KRW' => $currency->IDR_KRW,
      //     'IDR_USD' => $currency->IDR_USD,
      //     'KRW_IDR' => $currency->KRW_IDR,
      //     'KRW_USD' => $currency->KRW_USD,
      //     'USD_IDR' => $currency->USD_IDR,
      //     'USD_KRW' => $currency->USD_KRW,
      //     'last_update' => $currency->last_update
      //   ];
      // }

      // dd(session()->all());

      return view('website/home');
    }

    public function login()
    {
      if(session()->has('login_token')){
        return redirect(url('/profile/'.decrypt(session()->get('login_token'))));
      }
      return view('website/login');
    }

    public function signup()
    {
      if(session()->has('login_token')){
        return redirect(url('/profile/'.decrypt(session()->get('login_token'))));
      }
      return view('website/signup');
    }
}
