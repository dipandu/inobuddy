<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\DB;
use Illuminate\Routing\UrlGenerator;
use Validator;
use App\Models\Users as Users;
use App\Models\Users_meta as Users_meta;
/**
 * User API Controller
 */
class ApiUserController extends Controller
{
  public function check_user_exist(Request $request)
  {
    if(!empty($request->input('email'))){
      $user = Users::where('user_email', $request->input('email'))
                      ->get();

      if (!$user->isEmpty()) {
        $result = 'true';
      }else{
        $result = 'false';
      }
    }else{
      $user = Users::where('user_nicename', $request->input('username'))
                      ->get();

      if (!$user->isEmpty()) {
        $result = 'true';
      }else{
        $result = 'false';
      }
    }

    return $result;
  }

  public function signup(Request $request)
  {
    if(preg_match("/^[\w\.]+@/", $request->input('username')))
    {
      $result = [
        'status' => false,
        'message' => [0 => 'Username cannot contain special charaters']
      ];

      return $result;
      die();
    }

    $validator = Validator::make($request->input(),
      [
        'username' => 'required|unique:users,user_nicename,user_login',
        'password' => 'required|min:8|confirmed',
        'email' => 'required|email|unique:users,user_email'
      ]
    );

    if($validator->passes()){
      $user_data = [
        'user_login' => $request->input('username'),
        'user_nicename' => $request->input('username'),
        'user_pass' => encrypt($request->input('password')),
        'user_email' => $request->input('email'),
        'user_status' => 0,
        'created_at' => date('Y-m-d h:i:s')
      ];

      $insert_user = Users::insert($user_data);
      if($insert_user){
        $result = [
          'status' => true
        ];
      }

      $user_id = Users::where('user_nicename', $request->input('username'))->first();

      $credentials = [
        'login_token' => encrypt($request->input('username')),
        'uid' => encrypt($user_id->id)
      ];

      session($credentials);
    }else{
      $result = [
        'status' => false,
        'message' => $validator->messages()
      ];
    }

    return $result;
  }

  public function login_authentication(Request $request)
  {
    $validator = Validator::make($request->input(),
      [
        'username' => 'required',
        'password' => 'required|min:8'
      ]
    );

    if($validator->passes()){
      if(api_get(url('api/check-user-exist').'?username='.$request->input('username'))){
        $check_user_pwd = Users::where('user_login', $request->input('username'))
        ->first();

        if($request->input('password') == decrypt($check_user_pwd->user_pass)){
          $credentials = [
            'login_token' => encrypt($check_user_pwd->user_nicename),
            'uid' => encrypt($check_user_pwd->id)
          ];

          session($credentials);

          // dd($check_user_pwd->user_nicename);

          if(!empty($request->input('from'))){
            $result = [
              'status' => true,
              'login_token' => encrypt($check_user_pwd->user_nicename),
              'next_page' => $request->input('from')
            ];
          }else{
            $result = [
              'status' => true,
              'login_token' => encrypt($check_user_pwd->user_nicename),
              'next_page' => url('profile/'.$check_user_pwd->user_nicename)
            ];
          }
        }else{
          $result = [
            'status' => false,
            'message' => [
              0 => 'Your password is not match with our record!'
            ]
          ];
        }
      }elseif (api_get(url('api/check-user-exist').'?email='.$request->input('username'))){
        $check_user_pwd = Users::where('user_email', $request->input('username'))
        ->first();

        if($request->input('password') == decrypt($check_user_pwd->user_pass)){
          $credentials = [
            'login_token' => encrypt($check_user_pwd->user_nicename),
            'uid' => encrypt($check_user_pwd->id)
          ];

          session($credentials);

          // dd($check_user_pwd->user_nicename);
          if(!empty($request->input('from'))){
            $result = [
              'status' => true,
              'login_token' => encrypt($check_user_pwd->user_nicename),
              'next_page' => $request->input('from')
            ];
          }else{
            $result = [
              'status' => true,
              'login_token' => encrypt($check_user_pwd->user_nicename),
              'next_page' => url('profile/'.$check_user_pwd->user_nicename)
            ];
          }
        }else{
          $result = [
            'status' => false,
            'message' => [
              0 => 'Your password is not match with our record!'
            ]
          ];
        }
      }else{
        $result = [
          'status' => false,
          'message' => [
            0 => 'We cannot find your account!'
          ]
        ];
      }
    }else{
      $result = [
        'status' => false,
        'message' => $validator->messages()
      ];
    }

    return $result;
  }

  public function save_user_data(Request $request)
  {
    $user = Users::where('user_nicename', decrypt(session()->get('login_token')))
    ->first();

    $validation_rules = [
      // 'email' => 'required|min:5|email|unique:users,user_email',
      'first_name' => 'required',
      'last_name' => 'required',
      'country' => 'required',
      'phone' => 'required|numeric',
      'business_category' => 'required'
    ];

    if($request->hasFile('profile_picture')){
      $validation_rules['profile_picture'] = 'mimes:jpg,png,jpeg';
    }

    $validator = Validator::make($request->input(), $validation_rules);

    if($validator->passes()){
      if($request->hasFile('profile_picture')){
        $file = $request->file('profile_picture')->storeAs(decrypt(session()->get('login_token')).'/ppic', '__profile_picture.png', 'public');
      }

      $user_meta_received = [
        'user_first_name' => $request->input('first_name'),
        'user_last_name' => $request->input('last_name'),
        'user_country' => $request->input('country'),
        'user_phone' => $request->input('phone'),
        'user_organization_name' => $request->input('organization_name'),
        'user_business_category' => $request->input('business_category')
      ];

      // if($request->input('email') != $user->user_email){
      //   $update_email = Users::where('user_nicename', decrypt(session()->get('login_token')))
      //   ->update
      // }

      // dd($user->user_status);

      if($user->user_status == 0){
        foreach ($user_meta_received as $dk => $dv) {
          $insert = Users_meta::insert(array('users_id' => $user->id, 'meta_key' => $dk, 'meta_value' => $dv));
        }

        if($insert){
          if(Users::where('user_nicename', decrypt(session()->get('login_token')))->update(['user_status' => 1])){
            $result = [
              'status' => true
            ];
          }else{
            $result = [
              'status' => false,
              'message' => [0=>'Failed to change your status!']
            ];
          }
        }else{
          $result = [
            'status' => false,
            'message' => [0=>'Failed to save your data! A1']
          ];
        }
      }else{
        foreach ($user_meta_received as $dk => $dv) {
          $update = Users_meta::where('meta_key', $dk)->where('users_id', $user->id)->update(['meta_value' => $dv]);
        }

        $result = [
          'status' => true
        ];
      }
    }else{
      $result = [
        'status' => false,
        'message' => $validator->messages()
      ];
    }

    return $result;
  }
}
