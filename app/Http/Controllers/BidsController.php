<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\DB;
use Illuminate\Routing\UrlGenerator;
use Validator;
use App\Models\Bids as Bids;
use App\Models\Users as Users;
use App\Models\Tenders as Tenders;
use App\Models\Tenders_meta as Tenders_meta;
use App\Models\Users_meta as Users_meta;
/**
 * User API Controller
 */
class BidsController extends Controller
{
  public function placeBid(Request $request)
  {
    if(!session()->has('login_token')){
      return [
        'status' => false,
        'messages' => [
          'permission_error' => 'You have to login first!'
        ]
      ];
    }

    $validation_rules = [
      'price' => 'required|integer',
      'days' => 'required|integer|digits_between:0,3',
      'description' => 'required|min:15',
      'ttb' => 'required|exists:tenders,id'
    ];

    $validator = Validator::make($request->input(), $validation_rules);

    if($validator->passes()){
      $bid = new Bids;
      $bid->bid_price = $request->input('price');
      $bid->bid_days_to_deliver = $request->input('days');
      $bid->bid_description = $request->input('description');
      $bid->bid_creator = decrypt(session()->get('uid'));
      $bid->bid_tender = $request->input('ttb');
      
      if($bid->save()){
        return [
            'status' => true
        ];
      }else{
        return [
            'status' => false,
            'messages' => [
                'insert_error' => 'Failed to place the bid, Please contact administrator!' 
            ]
        ];
      }

    }else{
      return [
        'status' => false,
        'messages' => $validator->messages()
      ];
    }
  }

  public static function getBidsByTender($tenderId)
  {
    $bids_data = Bids::where('bid_tender', $tenderId)
    ->leftJoin('users', 'users.id', '=', 'bids.bid_creator')
    ->leftJoin('tenders', 'tenders.id', '=', 'bids.bid_tender')
    ->select('bids.*', 'users.user_nicename', 'tenders.tender_title', 'tenders.tender_slug')
    ->get();

    if(!$bids_data->isEmpty()){
      return $bids_data->toArray();
    }else{
      return [];
    }
  }

  public static function getBidsByUser($creatorId)
  {
    $bids_data = Bids::where('bid_creator', $creatorId)
    ->leftJoin('users', 'users.id', '=', 'bids.bid_creator')
    ->leftJoin('tenders', 'tenders.id', '=', 'bids.bid_tender')
    ->select('bids.*', 'users.user_nicename', 'tenders.tender_title', 'tenders.tender_slug', 'tenders.tender_currency')
    ->get();

    if(!$bids_data->isEmpty()){
      return $bids_data->toArray();
    }else{
      return [];
    }
  }

  public function changeStatus(Request $request)
  {
    if(!session()->has('login_token')){
      return abort(404);
    }

    $bid = Bids::find($request->input('bid'));
    $bid->bid_status = $request->input('status');
    if($bid->save()){
      $result = [
        'status' => true
      ];
    }else{
      $result = [
        'status' => false,
        'message' => [
          0 => 'Failed to update the status'
        ]
      ];
    }
    return $result;
  }
}
