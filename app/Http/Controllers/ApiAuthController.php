<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\DB;
use Illuminate\Routing\UrlGenerator;
use Validator;
use Socialite;
use Guzzle;
use App\Models\Users as Users;

class ApiAuthController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider(Request $request)
    {
        if(!empty($request->input('from'))){
          $previous_page = ['previous_page' => $request->input('from')];
          session($previous_page);
        }

        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request)
    {
        $user = Socialite::driver('google')->user();
        // dd(api_get(url('/api/check-user-exist').'?email='.$user->getEmail()));
        if(api_get(url('/api/check-user-exist').'?email='.$user->getEmail())){
          $existing_user = Users::where('user_email', $user->getEmail())
                  ->first();

          $credentials = [
            'login_token' => encrypt($existing_user->user_nicename),
            'uid' => encrypt($existing_user->id)
          ];

          session($credentials);

          if(session()->has('previous_page')){
            $url = session()->pull('previous_page', 'default');
            return redirect($url);
          }else{
            return redirect(url('/profile/'));
          }
        }else{
          $new_user = [
            'user_login' => $user->getId(),
            'user_nicename' => $user->getId(),
            'user_pass' => encrypt($user->getId()),
            'user_email' => $user->getEmail(),
            'user_status' => 0,
            'created_at' => date('Y-m-d h:i:s')
          ];

          if($insert_user = Users::insert($new_user)){
            $user_id = Users::where('user_nicename', $request->input('username'))->first();
            
            $credentials = [
              'login_token' => encrypt($user->getId()),
              'uid' => encrypt($user_id->id)
            ];

            session($credentials);

            $tmp_profile = [
              'tmp_user_data' => [
                'user_name' => $user->getName()
              ]
            ];

            session($tmp_profile);
            if(session()->has('previous_page')){
              $url = session()->pull('previous_page', 'default');
              // dd($url);
              return redirect($url);
            }else{
              return redirect(url('/profile/'));
            }
          }
        }
    }

    public function login()
    {
      dd("ea");
    }
}
