<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    //
    public $timestamps = true;
    protected $table = 'users';

    public function user_meta()
    {
      return $this->hasOne('App\Models\Users_meta', 'users_id', 'id');
    }

    public function get_userId($user_nicename)
    {
      $user_id = $this::where('user_nicename', $user_nicename)
      ->first();

      if($user_id){
        $result = $user_id->id;
      }else{
        $result = null;
      }

      return $result;
    }
}
