<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tenders_meta;
use Carbon\Carbon;

class Tenders extends Model
{
    //
    public $timestamps = true;
    protected $table = 'tenders';

    public function tender_meta()
    {
      return $this->hasMany('App\Models\Tenders_meta', 'tenders_id', 'id');
    }

    public function get_user_tender($user_id)
    {
      $tenders = $this::where('tender_creator', $user_id)
      ->whereIn('tender_status', ['0', '1', '2'])
      ->get();

      if($tenders->isEmpty()){
        $tenders = (object) [];
      }

      return $tenders;
    }

    public function get_tenderById($tender_id)
    {
      $tender = $this::where('id', $tender_id)
      ->whereIn('tender_status', ['0', '1', '2'])
      ->first();

      if(!$tender){
        $tender = (object) [];
      }

      return $tender;
    }

    public function get_browseTender($where, $limitPerPage)
    {
      $tenders_parent = $this::whereIn('tender_status', ['1']);
      $tenders_parent->where('tender_deadline', '>', Carbon::now());
      foreach ($where as $wk => $wv) {
        switch ($wv['key']) {
          case 'currency':
            $tenders_parent->where('tender_currency', $wv['value']);
            break;

          case 'price_range':
            $tenders_parent->whereBetween('tender_budget', $wv['value']);
            break;

          case 'query':
            $tenders_parent->where('tender_title', 'like', '%'.$wv['value'].'%');
            $tenders_parent->orWhere('tender_category', 'like', '%'.$wv['value'].'%');
            break;

          default:
            #do nothing
            break;
        }
      }

      $tenders_parent_data = $tenders_parent->paginate($limitPerPage);

      foreach ($tenders_parent_data as $key => $tender) {
        $tenders_result[$tender->id] = $tender->toArray();
      }

      if(empty($tenders_result)){
        $tenders_result = [];
      }

      $price_query = $this::where('tender_status', ['1']);
      $price_query->where('tender_deadline', '>', Carbon::now());
      $price_query->orderBy('tender_budget', 'desc');
      foreach ($where as $wk => $wv) {
        switch ($wv['key']) {
          case 'currency':
            $price_query->where('tender_currency', $wv['value']);
            break;

          case 'query':
            $price_query->where('tender_title', 'like', '%'.$wv['value'].'%');
            $price_query->orWhere('tender_category', 'like', '%'.$wv['value'].'%');
            break;

          default:
            #do nothing
            break;
        }
      }

      $lowest_price = $price_query->min('tender_budget');
      $highest_price = $price_query->max('tender_budget');

      if(empty($price_query->min('tender_budget')) or empty($price_query->max('tender_budget'))){
        $lowest_price = 0;
        $highest_price = 0;
      }

      $result = [
        'tenders' => $tenders_result,
        'lowest_price' => $lowest_price,
        'highest_price' => $highest_price,
        'paginator' => $tenders_parent_data,
        'currentPage' => $tenders_parent_data->currentPage(),
        'lastPage' => $tenders_parent_data->lastPage()
      ];

      return $result;
    }

}
