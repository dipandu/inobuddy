<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bids extends Model
{
    //
    public $timestamps = true;
    protected $table = 'bids';
}
