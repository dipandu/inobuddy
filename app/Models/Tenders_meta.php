<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tenders;

class Tenders_meta extends Model
{
    //
    public $timestamps = false;
    protected $table = 'tenders_meta';

    public function tender()
    {
        return $this->belongsTo('App\Models\Tenders_meta', 'id', 'tenders_id');
    }
}
