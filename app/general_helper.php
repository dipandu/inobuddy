<?php
ini_set('default_charset', 'UTF-8');
// defined('BASEPATH') OR exit('No direct script access allowed');
if (!function_exists('safe_url')) {
	function safe_url ($string) {
		return strtolower(preg_replace('/[^a-zA-Z0-9,]^-+|-+$/', '-', htmlentities($string)));
	}
}

if(!function_exists('dp_slug_maker')){
  function dp_slug_maker($string){
    $string = preg_replace('~[^\\pL\d]+~u', '-', $string);
    $string = trim($string, '-');
    $string = strtolower($string);
    $string = preg_replace('~[^-\w]+~', '', $string);
    return $string;
  }
}

if(!function_exists('baba_img_cdn')){
  function img_cdn($imgUrl){
	 	$imgUrl_ready = "https://cdn.ebaba.co.id/".$imgUrl;
    return $imgUrl_ready;
  }
}
if(!function_exists('fetch_size')){
  function fetch_size($data = 'Raw data'){
	 	$replace = str_replace([
							'{','}','"'
							],	'',	$data);
			$explode_a = explode(',',	$replace);
			foreach	($explode_a as $a){
					$explode_b[] = explode(':',	$a);
//					$result[]=$a[1];
			}
			foreach	($explode_b as $b){
					$result[$b[0]]=$b[1];
			}
    return $result;
  }
}
if(!function_exists('sorter_lib')){
  function sorter_lib($table = '', $field_name = '', $param){
    if(!empty($table)){
      $table = $table.".";
    }

    if(strtoupper($param) == "ASC" or strtoupper($param) == "DESC"){
      switch  (strtoupper($param)){
         case 'ASC':
            $opt = "ASC";
         break;
         case 'DESC':
            $opt = "DESC";
         break;
      }

      return $table."`".$field_name."` ".$opt;
    }else{
        return $table."`id` ASC";
    }
  }

}
if(!function_exists('meta_product')){
  function meta_product($config){
				$result = "<meta property='fb:app_id'                      content='1588060758157172' />
               <meta property='og:type'                        content='product.item' />
               <meta property='og:url'                         content='".base_url('product/'.$config['slug'])."' />
               <meta property='og:title'                       content='eBaba Shop | ".$config['title']."' />
               <meta property='og:description'            content='".$config['description']."' />
               <meta property='product:retailer_item_id'       content='".$config['product_id']."' />
               <meta property='product:price:amount'           content='".$config['price']."' />
               <meta property='product:price:currency'         content='IDR' />
               <meta property='product:sale_price:amount'      content='".$config['final_price']."' />
               <meta property='product:sale_price:currency'    content='IDR' />";
				return $result;
		}
}
if(!function_exists('render_additonal_fields')){
   function render_additonal_fields($data){
      $raw = json_decode($data);

      foreach ($raw as $i => $v){
         if ($i == 'name') {
            $name = json_decode($v);
         }
         if ($i == 'value') {
            $value = json_decode($v);
         }
      }

      if ($name == false || $value == false) {
         return array();
      }

      foreach ($name as $n => $row) {
         $final[] = array(
            'name' => $row,
            'value' => $value[$n]
         );
      }
      return $final;
		}
}

function api_get($url = NULL){
	if(!empty($url)){
		$curl = curl_init();
		curl_setopt($curl,CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_POST, FALSE);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($curl,CURLOPT_MAXREDIRS, 10);
		curl_setopt($curl,CURLOPT_TIMEOUT, 30);
		$result = curl_exec($curl);
		curl_close($curl);
	}else{
		$result = ["status"=>"200", "result"=>""];
	}
	return json_decode($result);
}

if(!function_exists('api_request')){
  function api_request($data, $url, $method = "POST / GET / URL"){
        if($method == "POST"){
            $curl = curl_init();
            curl_setopt($curl,CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, TRUE);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl,CURLOPT_MAXREDIRS, 10);
            curl_setopt($curl,CURLOPT_TIMEOUT, 30);
            $result = curl_exec($curl);
            curl_close($curl);
        }elseif($method == "GET"){
            $data = http_build_query($data);
            $curl = curl_init();
            curl_setopt($curl,CURLOPT_URL, $url."?".$data);
            curl_setopt($curl, CURLOPT_POST, FALSE);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl,CURLOPT_MAXREDIRS, 10);
            curl_setopt($curl,CURLOPT_TIMEOUT, 30);
            $result = curl_exec($curl);
            curl_close($curl);
        }elseif($method == "URL"){
            $curl = curl_init();
            curl_setopt($curl,CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, FALSE);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl,CURLOPT_MAXREDIRS, 10);
            curl_setopt($curl,CURLOPT_TIMEOUT, 30);
            $result = curl_exec($curl);
            curl_close($curl);
        }else{

        }
        return json_decode($result);
    }
}

function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

if(!function_exists('get_ongkir_data')){
   function get_ongkir_data($type = "province / city / cost", $param = "Fill with parameter from http://rajaongkir.com/dokumentasi/starter"){

      $curl = curl_init();

      if(strtolower($type) === "province"){
         if(!empty($param) or $param != ''){
            $prov_url = "http://pro.rajaongkir.com/api/province?".$param;
         }else{
            $prov_url = "http://pro.rajaongkir.com/api/province";
         }
         curl_setopt_array($curl, array(
            CURLOPT_URL => $prov_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
              "key: 4ea9909438857a96211c957d18c4810d"
            ),
         ));
      }else if(strtolower($type) === "city"){
         curl_setopt_array($curl, array(
           CURLOPT_URL => "http://pro.rajaongkir.com/api/city?".$param,
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 30,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => "GET",
           CURLOPT_HTTPHEADER => array(
             "key: 4ea9909438857a96211c957d18c4810d"
           ),
         ));
      }else if(strtolower($type) === "cost"){
         curl_setopt_array($curl, array(
           CURLOPT_URL => "http://pro.rajaongkir.com/api/cost",
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 30,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => "POST",
           CURLOPT_POSTFIELDS => $param,
           CURLOPT_HTTPHEADER => array(
             "content-type: application/x-www-form-urlencoded",
             "key: 4ea9909438857a96211c957d18c4810d"
           ),
         ));
      }else if(strtolower($type) === "subdistrict"){
         curl_setopt_array($curl, array(
           CURLOPT_URL => "http://pro.rajaongkir.com/api/subdistrict?".$param,
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 30,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => "GET",
           CURLOPT_HTTPHEADER => array(
             "key: 4ea9909438857a96211c957d18c4810d"
           ),
         ));
      }

      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);
      if ($err) {
        return "cURL Error #:" . $err;
      } else {
        return $response;
      }
   }
}


if(!function_exists('dp_money_format')){
  function dp_money_format($string){
    $formatted = $_SESSION['company_details']->currency ." ". number_format($string,0,',','.');
    return $formatted;
  }
}

if(!function_exists('activation_email')){
  function activation_email($name, $activation_link){
    $mail = 'Hi '.$name.'<br />';
    $mail.= '<p>I would like to welcome you to PortMeet.Com. This web site is the home of the traders community on the web and we are glad to have you as a member.</p>';
    $mail.= '<p>Please visit the following link in order to activate your account:<br />';
    $mail.= '<a href="'.$activation_link.'" title="Activate your account now!" target="_blank">Click here to activate!</a></p>';
    $mail.= '<p>Your password has been securely stored in our database and cannot be
      retrieved. In the event that it is forgotten, you will be able to reset it
      using the email address associated with your account.</p>';
    $mail.= '<p>Thank you for registering.</p>';

    return $mail;
  }
}

if(!function_exists('get_callsign')){
   function get_callsign($string){
      $acronym="";
      $word="";
      $words = ($string);

      if(preg_match('/^[a-z0-9 .\-]+$/i', $string)){
        if(count($words) > 2){
          foreach($words as $w) {
              $acronym .= substr($w,0,1);
          }
          $word = $word.$acronym;
        }else{
          $word = ucfirst(substr($string,0,2));
        }

        return $word;
      }else{
        return $string;
      }
    }
}

if(!function_exists('alpnum')){
   function alpnum($string){
      if(!ctype_alnum($string)){
        $result = mb_convert_encoding($string, "HTML-ENTITIES", "UTF-8");
      }else{
        $result = $string;
      }

      return $result;
    }
}

if(!function_exists('res_int')){
   function res_int($string){
      if(is_numeric($string)){
        if(empty($string)){
          $result = "0";
        }else{
          $result = $string;
        }
      }else{
        $result = $string;
      }

      return $result;
    }
}

if(!function_exists('rep_dash')){
   function rep_dash($string){
      if(empty($string)){
        $result = "-";
      }else{
        $result = $string;
      }

      return $result;
    }
}

if(!function_exists('url_friendly')){
  function url_friendly($data = NULL){
    if(!empty($data) or $data != NULL){
        return urlencode($data["id"]."-".strtolower(str_replace(" ", "_", $data["subject"])));
    }

    return NULL;
  }
}
